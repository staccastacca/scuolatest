<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Adds start_time and sent
 */
class Version20161127123523 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('ALTER TABLE fisica ADD start_time TIMESTAMP(0) WITHOUT TIME ZONE ');
            $this->addSql('ALTER TABLE fisica ADD sent BOOLEAN ');
            $this->addSql('ALTER TABLE italiano ADD start_time TIMESTAMP(0) WITHOUT TIME ZONE ');
            $this->addSql('ALTER TABLE italiano ADD sent BOOLEAN');
            $this->addSql('ALTER TABLE matematica ADD start_time TIMESTAMP(0) WITHOUT TIME ZONE ');
            $this->addSql('ALTER TABLE matematica ADD sent BOOLEAN ');
            $this->addSql('UPDATE matematica SET start_time=\'today\', sent=\'true\'');
            $this->addSql('UPDATE italiano SET start_time=\'today\', sent=\'true\'');
            $this->addSql('UPDATE fisica SET start_time=\'today\', sent=\'true\'');
            $this->addSql('ALTER TABLE fisica ALTER start_time SET NOT NULL');
            $this->addSql('ALTER TABLE fisica ALTER sent SET NOT NULL');
            $this->addSql('ALTER TABLE italiano ALTER start_time SET NOT NULL');
            $this->addSql('ALTER TABLE italiano ALTER sent SET NOT NULL');
            $this->addSql('ALTER TABLE matematica ALTER start_time SET NOT NULL');
            $this->addSql('ALTER TABLE matematica ALTER sent SET NOT NULL');
        }
        else{
            $this->addSql('ALTER TABLE fisica ADD start_time DATETIME, ADD sent TINYINT(1)');
            $this->addSql('ALTER TABLE italiano ADD start_time DATETIME, ADD sent TINYINT(1)');
            $this->addSql('ALTER TABLE matematica ADD start_time DATETIME, ADD sent TINYINT(1)');
            $this->addSql('UPDATE matematica SET start_time=UTC_TIMESTAMP(), sent=1');
            $this->addSql('UPDATE italiano SET start_time=UTC_TIMESTAMP(), sent=1');
            $this->addSql('UPDATE fisica SET start_time=UTC_TIMESTAMP(), sent=1');
            $this->addSql('ALTER TABLE fisica CHANGE start_time start_time DATETIME NOT NULL, CHANGE sent sent TINYINT(1) NOT NULL');
            $this->addSql('ALTER TABLE italiano CHANGE start_time start_time DATETIME NOT NULL, CHANGE sent sent TINYINT(1) NOT NULL');
            $this->addSql('ALTER TABLE matematica CHANGE start_time start_time DATETIME NOT NULL, CHANGE sent sent TINYINT(1) NOT NULL');
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('ALTER TABLE fisica DROP start_time');
            $this->addSql('ALTER TABLE fisica DROP sent');
            $this->addSql('ALTER TABLE italiano DROP start_time');
            $this->addSql('ALTER TABLE italiano DROP sent');
            $this->addSql('ALTER TABLE matematica DROP start_time');
            $this->addSql('ALTER TABLE matematica DROP sent');
        }
        else{
            $this->addSql('ALTER TABLE fisica DROP start_time, DROP sent');
            $this->addSql('ALTER TABLE italiano DROP start_time, DROP sent');
            $this->addSql('ALTER TABLE matematica DROP start_time, DROP sent');
        }
    }
}
