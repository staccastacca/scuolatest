<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Adds columns to italiano
 */
class Version20161130151112 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('ALTER TABLE italiano ADD virgola_b VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD virgola_c VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD lessico_a VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD lessico_b VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD lessico_c VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD lessico_d VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD lessico_e VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD lessico_f VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD lessico_g VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD lessico_h VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD lessico_i VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD lessico_j VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD plurale_cane VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano RENAME COLUMN pluralecane TO virgola_a');
        }
        else{
            $this->addSql('ALTER TABLE italiano ADD virgola_b VARCHAR(255) DEFAULT NULL, ADD virgola_c VARCHAR(255) DEFAULT NULL, ADD lessico_a VARCHAR(255) DEFAULT NULL, ADD lessico_b VARCHAR(255) DEFAULT NULL, ADD lessico_c VARCHAR(255) DEFAULT NULL, ADD lessico_d VARCHAR(255) DEFAULT NULL, ADD lessico_e VARCHAR(255) DEFAULT NULL, ADD lessico_f VARCHAR(255) DEFAULT NULL, ADD lessico_g VARCHAR(255) DEFAULT NULL, ADD lessico_h VARCHAR(255) DEFAULT NULL, ADD lessico_i VARCHAR(255) DEFAULT NULL, ADD lessico_j VARCHAR(255) DEFAULT NULL, ADD plurale_cane VARCHAR(255) DEFAULT NULL, CHANGE pluralecane virgola_a VARCHAR(255) DEFAULT NULL');
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('ALTER TABLE italiano ADD pluralecane VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano DROP virgola_a');
            $this->addSql('ALTER TABLE italiano DROP virgola_b');
            $this->addSql('ALTER TABLE italiano DROP virgola_c');
            $this->addSql('ALTER TABLE italiano DROP lessico_a');
            $this->addSql('ALTER TABLE italiano DROP lessico_b');
            $this->addSql('ALTER TABLE italiano DROP lessico_c');
            $this->addSql('ALTER TABLE italiano DROP lessico_d');
            $this->addSql('ALTER TABLE italiano DROP lessico_e');
            $this->addSql('ALTER TABLE italiano DROP lessico_f');
            $this->addSql('ALTER TABLE italiano DROP lessico_g');
            $this->addSql('ALTER TABLE italiano DROP lessico_h');
            $this->addSql('ALTER TABLE italiano DROP lessico_i');
            $this->addSql('ALTER TABLE italiano DROP lessico_j');
            $this->addSql('ALTER TABLE italiano DROP plurale_cane');
        }
        else{
            $this->addSql('ALTER TABLE italiano ADD pluralecane VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, DROP virgola_a, DROP virgola_b, DROP virgola_c, DROP lessico_a, DROP lessico_b, DROP lessico_c, DROP lessico_d, DROP lessico_e, DROP lessico_f, DROP lessico_g, DROP lessico_h, DROP lessico_i, DROP lessico_j, DROP plurale_cane');
        }
    }
}
