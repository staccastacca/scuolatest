<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Adds columns for second question in italian test 
 */
class Version20161129155248 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('ALTER TABLE italiano ADD punteggiatura_a VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD punteggiatura_b VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD punteggiatura_c VARCHAR(255) DEFAULT NULL');
        }
        else{
            $this->addSql('ALTER TABLE italiano ADD punteggiatura_a VARCHAR(255) DEFAULT NULL, ADD punteggiatura_b VARCHAR(255) DEFAULT NULL, ADD punteggiatura_c VARCHAR(255) DEFAULT NULL');
        }

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('ALTER TABLE italiano DROP punteggiatura_a');
            $this->addSql('ALTER TABLE italiano DROP punteggiatura_b');
            $this->addSql('ALTER TABLE italiano DROP punteggiatura_c');
        }
        else{
            $this->addSql('ALTER TABLE italiano DROP punteggiatura_a, DROP punteggiatura_b, DROP punteggiatura_c');
        }
    }
}
