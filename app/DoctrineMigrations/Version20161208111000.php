<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161208111000 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('ALTER TABLE italiano ADD marks TEXT DEFAULT NULL');
            $this->addSql('COMMENT ON COLUMN italiano.marks IS \'(DC2Type:array)\'');
            $this->addSql('ALTER TABLE matematica ADD marks TEXT DEFAULT NULL');
            $this->addSql('COMMENT ON COLUMN matematica.marks IS \'(DC2Type:array)\'');
            $this->addSql('ALTER TABLE comprensione ADD marks TEXT DEFAULT NULL');
            $this->addSql('COMMENT ON COLUMN comprensione.marks IS \'(DC2Type:array)\'');
            $this->addSql('ALTER TABLE fisica ADD marks TEXT DEFAULT NULL');
            $this->addSql('COMMENT ON COLUMN fisica.marks IS \'(DC2Type:array)\'');
            $this->addSql('ALTER TABLE users ALTER salt DROP NOT NULL');
        }
        else{
            $this->addSql('ALTER TABLE fisica ADD marks LONGTEXT NULL COMMENT \'(DC2Type:array)\'');
            $this->addSql('ALTER TABLE comprensione ADD marks LONGTEXT NULL COMMENT \'(DC2Type:array)\'');
            $this->addSql('ALTER TABLE italiano ADD marks LONGTEXT NULL COMMENT \'(DC2Type:array)\'');
            $this->addSql('ALTER TABLE matematica ADD marks LONGTEXT NULL COMMENT \'(DC2Type:array)\'');
        }
        $this->addSql('UPDATE comprensione SET marks=\'a:0:{}\'');
        $this->addSql('UPDATE fisica SET marks=\'a:0:{}\'');
        $this->addSql('UPDATE italiano SET marks=\'a:0:{}\'');
        $this->addSql('UPDATE matematica SET marks=\'a:0:{}\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('CREATE SCHEMA public');
            $this->addSql('ALTER TABLE comprensione DROP marks');
            $this->addSql('ALTER TABLE users ALTER salt SET NOT NULL');
            $this->addSql('ALTER TABLE matematica DROP marks');
            $this->addSql('ALTER TABLE fisica DROP marks');
            $this->addSql('ALTER TABLE italiano DROP marks');
        }
        else{
            $this->addSql('ALTER TABLE comprensione DROP marks');
            $this->addSql('ALTER TABLE fisica DROP marks');
            $this->addSql('ALTER TABLE italiano DROP marks');
            $this->addSql('ALTER TABLE matematica DROP marks');
        }
    }
}
