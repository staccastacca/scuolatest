<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161205111638 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        $this->addSql('ALTER TABLE italiano DROP punteggiatura_c');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        $this->addSql('ALTER TABLE italiano ADD punteggiatura_c VARCHAR(255) DEFAULT NULL');
    }
}
