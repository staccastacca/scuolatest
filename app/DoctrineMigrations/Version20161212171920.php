<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Removing 'k' field from math comprehension test since is not used
 */
class Version20161212171920 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        $this->addSql('ALTER TABLE comprensione DROP k');
        $this->addSql('ALTER TABLE feedback ADD user_id INT');
        $this->addSql('UPDATE feedback SET user_id=\'0\'');
        if($dbPlatform == 'postgresql') $this->addSql('ALTER TABLE feedback ALTER user_id SET NOT NULL');
        else $this->addSql('ALTER TABLE feedback CHANGE user_id user_id DATETIME NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql') $this->addSql('ALTER TABLE comprensione ADD k VARCHAR(1) DEFAULT NULL');
        else $this->addSql('ALTER TABLE comprensione ADD k VARCHAR(1) DEFAULT NULL COLLATE utf8_unicode_ci');

        $this->addSql('ALTER TABLE feedback ADD user_id INT NOT NULL');
    }
}
