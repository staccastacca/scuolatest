<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Adds table comprensione (answers in maths are lost)
 */
class Version20161130154730 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('CREATE SEQUENCE comprensione_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
            $this->addSql('CREATE TABLE comprensione (id INT NOT NULL, sydney_berlino VARCHAR(1) DEFAULT NULL, ora_sydney TIME(0) WITHOUT TIME ZONE DEFAULT NULL, ora_berlino TIME(0) WITHOUT TIME ZONE DEFAULT NULL, moltiplicazione_indiana VARCHAR(255) DEFAULT NULL, k VARCHAR(1) DEFAULT NULL, user_id INT NOT NULL, mark INT DEFAULT NULL, start_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, sent BOOLEAN NOT NULL, PRIMARY KEY(id))');
            $this->addSql('ALTER TABLE matematica DROP sydney_berlino');
            $this->addSql('ALTER TABLE matematica DROP ora_sydney');
            $this->addSql('ALTER TABLE matematica DROP ora_berlino');
            $this->addSql('ALTER TABLE matematica DROP moltiplicazione_indiana');
            $this->addSql('ALTER TABLE matematica DROP k');
        }
        else{
            $this->addSql('CREATE TABLE comprensione (id INT AUTO_INCREMENT NOT NULL, sydney_berlino VARCHAR(1) DEFAULT NULL, ora_sydney TIME DEFAULT NULL, ora_berlino TIME DEFAULT NULL, moltiplicazione_indiana VARCHAR(255) DEFAULT NULL, k VARCHAR(1) DEFAULT NULL, user_id INT NOT NULL, mark INT DEFAULT NULL, start_time DATETIME NOT NULL, sent TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
            $this->addSql('ALTER TABLE matematica DROP sydney_berlino, DROP ora_sydney, DROP ora_berlino, DROP moltiplicazione_indiana, DROP k');
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('DROP SEQUENCE comprensione_id_seq CASCADE');
            $this->addSql('DROP TABLE comprensione');
            $this->addSql('ALTER TABLE matematica ADD sydney_berlino VARCHAR(1) DEFAULT NULL');
            $this->addSql('ALTER TABLE matematica ADD ora_sydney TIME(0) WITHOUT TIME ZONE DEFAULT NULL');
            $this->addSql('ALTER TABLE matematica ADD ora_berlino TIME(0) WITHOUT TIME ZONE DEFAULT NULL');
            $this->addSql('ALTER TABLE matematica ADD moltiplicazione_indiana VARCHAR(255) DEFAULT NULL');
            $this->addSql('ALTER TABLE matematica ADD k VARCHAR(1) DEFAULT NULL');
        }
        else{
            $this->addSql('DROP TABLE comprensione');
            $this->addSql('ALTER TABLE matematica ADD sydney_berlino VARCHAR(1) DEFAULT NULL COLLATE utf8_unicode_ci, ADD ora_sydney TIME DEFAULT NULL, ADD ora_berlino TIME DEFAULT NULL, ADD moltiplicazione_indiana VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD k VARCHAR(1) DEFAULT NULL COLLATE utf8_unicode_ci');
        }
    }
}
