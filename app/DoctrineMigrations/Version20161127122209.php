<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * First db migration
 */
class Version20161127122209 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('CREATE SEQUENCE fisica_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
            $this->addSql('CREATE SEQUENCE users_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
            $this->addSql('CREATE SEQUENCE italiano_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
            $this->addSql('CREATE SEQUENCE matematica_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
            $this->addSql('CREATE TABLE fisica (id INT NOT NULL, localita VARCHAR(1) DEFAULT NULL, luce VARCHAR(1) DEFAULT NULL, circuito_luminoso VARCHAR(1) DEFAULT NULL, circuito_esplode VARCHAR(1) DEFAULT NULL, user_id INT NOT NULL, mark INT DEFAULT NULL, PRIMARY KEY(id))');
            $this->addSql('CREATE TABLE users (id INT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled BOOLEAN NOT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_login TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, roles TEXT NOT NULL, email_checked BOOLEAN NOT NULL, PRIMARY KEY(id))');
            $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E992FC23A8 ON users (username_canonical)');
            $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9A0D96FBF ON users (email_canonical)');
            $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9C05FB297 ON users (confirmation_token)');
            $this->addSql('COMMENT ON COLUMN users.roles IS \'(DC2Type:array)\'');
            $this->addSql('CREATE TABLE italiano (id INT NOT NULL, due_punti_a VARCHAR(1) DEFAULT NULL, due_punti_b VARCHAR(1) DEFAULT NULL, due_punti_c VARCHAR(1) DEFAULT NULL, due_punti_d VARCHAR(1) DEFAULT NULL, due_punti_e VARCHAR(1) DEFAULT NULL, plurale_guerra VARCHAR(255) DEFAULT NULL, plurale_capo VARCHAR(255) DEFAULT NULL, pluralecane VARCHAR(255) DEFAULT NULL, plurale_buono VARCHAR(255) DEFAULT NULL, funzione_ga VARCHAR(1) DEFAULT NULL, funzione_gb VARCHAR(1) DEFAULT NULL, funzione_gc VARCHAR(1) DEFAULT NULL, funzione_la VARCHAR(1) DEFAULT NULL, funzione_lb VARCHAR(1) DEFAULT NULL, funzione_lc VARCHAR(1) DEFAULT NULL, funzione_sa VARCHAR(1) DEFAULT NULL, funzione_sb VARCHAR(1) DEFAULT NULL, funzione_sc VARCHAR(1) DEFAULT NULL, argomento_testo VARCHAR(1) DEFAULT NULL, dolcemente VARCHAR(1) DEFAULT NULL, non_cade VARCHAR(1) DEFAULT NULL, sequenza VARCHAR(1) DEFAULT NULL, tempo VARCHAR(1) DEFAULT NULL, spazio VARCHAR(1) DEFAULT NULL, narratore VARCHAR(1) DEFAULT NULL, buck VARCHAR(1) DEFAULT NULL, lessico VARCHAR(1) DEFAULT NULL, user_id INT NOT NULL, mark INT DEFAULT NULL, PRIMARY KEY(id))');
            $this->addSql('CREATE TABLE matematica (id INT NOT NULL, angoli VARCHAR(1) DEFAULT NULL, rombo VARCHAR(1) DEFAULT NULL, foglio_carta VARCHAR(1) DEFAULT NULL, semicerchio VARCHAR(1) DEFAULT NULL, vero_falso_geometria1 BOOLEAN DEFAULT NULL, vero_falso_geometria2 BOOLEAN DEFAULT NULL, vero_falso_geometria3 BOOLEAN DEFAULT NULL, vero_falso_geometria4 BOOLEAN DEFAULT NULL, relazioni VARCHAR(1) DEFAULT NULL, distanza_punti VARCHAR(1) DEFAULT NULL, triangoli VARCHAR(1) DEFAULT NULL, simmetria VARCHAR(1) DEFAULT NULL, circonferenza VARCHAR(1) DEFAULT NULL, fattori_primi VARCHAR(1) DEFAULT NULL, mcdormcm1 BOOLEAN DEFAULT NULL, mcdormcm2 BOOLEAN DEFAULT NULL, mcdormcm3 BOOLEAN DEFAULT NULL, mcdormcm4 BOOLEAN DEFAULT NULL, tab_num1 VARCHAR(255) DEFAULT NULL, tab_num2 VARCHAR(255) DEFAULT NULL, tab_num3 VARCHAR(255) DEFAULT NULL, tab_num4 VARCHAR(255) DEFAULT NULL, tab_num5 VARCHAR(255) DEFAULT NULL, tab_num6 VARCHAR(255) DEFAULT NULL, si_no_primi BOOLEAN DEFAULT NULL, si_no_primi_loro BOOLEAN DEFAULT NULL, un_mezzo VARCHAR(1) DEFAULT NULL, numero_razionale VARCHAR(1) DEFAULT NULL, medicina VARCHAR(1) DEFAULT NULL, cannavacciuolo VARCHAR(1) DEFAULT NULL, equazione VARCHAR(1) DEFAULT NULL, successivo VARCHAR(1) DEFAULT NULL, sydney_berlino VARCHAR(1) DEFAULT NULL, ora_sydney TIME(0) WITHOUT TIME ZONE DEFAULT NULL, ora_berlino TIME(0) WITHOUT TIME ZONE DEFAULT NULL, moltiplicazione_indiana VARCHAR(255) DEFAULT NULL, k VARCHAR(1) DEFAULT NULL, user_id INT NOT NULL, mark INT DEFAULT NULL, PRIMARY KEY(id))');
        }
        else{
            $this->addSql('CREATE TABLE fisica (id INT AUTO_INCREMENT NOT NULL, localita VARCHAR(1) DEFAULT NULL, luce VARCHAR(1) DEFAULT NULL, circuito_luminoso VARCHAR(1) DEFAULT NULL, circuito_esplode VARCHAR(1) DEFAULT NULL, user_id INT NOT NULL, mark INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
            $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', email_checked TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_1483A5E992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_1483A5E9A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_1483A5E9C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
            $this->addSql('CREATE TABLE italiano (id INT AUTO_INCREMENT NOT NULL, due_punti_a VARCHAR(1) DEFAULT NULL, due_punti_b VARCHAR(1) DEFAULT NULL, due_punti_c VARCHAR(1) DEFAULT NULL, due_punti_d VARCHAR(1) DEFAULT NULL, due_punti_e VARCHAR(1) DEFAULT NULL, plurale_guerra VARCHAR(255) DEFAULT NULL, plurale_capo VARCHAR(255) DEFAULT NULL, pluralecane VARCHAR(255) DEFAULT NULL, plurale_buono VARCHAR(255) DEFAULT NULL, funzione_ga VARCHAR(1) DEFAULT NULL, funzione_gb VARCHAR(1) DEFAULT NULL, funzione_gc VARCHAR(1) DEFAULT NULL, funzione_la VARCHAR(1) DEFAULT NULL, funzione_lb VARCHAR(1) DEFAULT NULL, funzione_lc VARCHAR(1) DEFAULT NULL, funzione_sa VARCHAR(1) DEFAULT NULL, funzione_sb VARCHAR(1) DEFAULT NULL, funzione_sc VARCHAR(1) DEFAULT NULL, argomento_testo VARCHAR(1) DEFAULT NULL, dolcemente VARCHAR(1) DEFAULT NULL, non_cade VARCHAR(1) DEFAULT NULL, sequenza VARCHAR(1) DEFAULT NULL, tempo VARCHAR(1) DEFAULT NULL, spazio VARCHAR(1) DEFAULT NULL, narratore VARCHAR(1) DEFAULT NULL, buck VARCHAR(1) DEFAULT NULL, lessico VARCHAR(1) DEFAULT NULL, user_id INT NOT NULL, mark INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
            $this->addSql('CREATE TABLE matematica (id INT AUTO_INCREMENT NOT NULL, angoli VARCHAR(1) DEFAULT NULL, rombo VARCHAR(1) DEFAULT NULL, foglio_carta VARCHAR(1) DEFAULT NULL, semicerchio VARCHAR(1) DEFAULT NULL, vero_falso_geometria1 TINYINT(1) DEFAULT NULL, vero_falso_geometria2 TINYINT(1) DEFAULT NULL, vero_falso_geometria3 TINYINT(1) DEFAULT NULL, vero_falso_geometria4 TINYINT(1) DEFAULT NULL, relazioni VARCHAR(1) DEFAULT NULL, distanza_punti VARCHAR(1) DEFAULT NULL, triangoli VARCHAR(1) DEFAULT NULL, simmetria VARCHAR(1) DEFAULT NULL, circonferenza VARCHAR(1) DEFAULT NULL, fattori_primi VARCHAR(1) DEFAULT NULL, mcdormcm1 TINYINT(1) DEFAULT NULL, mcdormcm2 TINYINT(1) DEFAULT NULL, mcdormcm3 TINYINT(1) DEFAULT NULL, mcdormcm4 TINYINT(1) DEFAULT NULL, tab_num1 VARCHAR(255) DEFAULT NULL, tab_num2 VARCHAR(255) DEFAULT NULL, tab_num3 VARCHAR(255) DEFAULT NULL, tab_num4 VARCHAR(255) DEFAULT NULL, tab_num5 VARCHAR(255) DEFAULT NULL, tab_num6 VARCHAR(255) DEFAULT NULL, si_no_primi TINYINT(1) DEFAULT NULL, si_no_primi_loro TINYINT(1) DEFAULT NULL, un_mezzo VARCHAR(1) DEFAULT NULL, numero_razionale VARCHAR(1) DEFAULT NULL, medicina VARCHAR(1) DEFAULT NULL, cannavacciuolo VARCHAR(1) DEFAULT NULL, equazione VARCHAR(1) DEFAULT NULL, successivo VARCHAR(1) DEFAULT NULL, sydney_berlino VARCHAR(1) DEFAULT NULL, ora_sydney TIME DEFAULT NULL, ora_berlino TIME DEFAULT NULL, moltiplicazione_indiana VARCHAR(255) DEFAULT NULL, k VARCHAR(1) DEFAULT NULL, user_id INT NOT NULL, mark INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('CREATE SCHEMA public');
            $this->addSql('DROP SEQUENCE fisica_id_seq CASCADE');
            $this->addSql('DROP SEQUENCE users_id_seq CASCADE');
            $this->addSql('DROP SEQUENCE italiano_id_seq CASCADE');
            $this->addSql('DROP SEQUENCE matematica_id_seq CASCADE');
            $this->addSql('DROP TABLE fisica');
            $this->addSql('DROP TABLE users');
            $this->addSql('DROP TABLE italiano');
            $this->addSql('DROP TABLE matematica');
        }
        else{
            $this->addSql('DROP TABLE fisica');
            $this->addSql('DROP TABLE users');
            $this->addSql('DROP TABLE italiano');
            $this->addSql('DROP TABLE matematica');
        }
    }
}
