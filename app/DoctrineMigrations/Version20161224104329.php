<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Adds table for english test
 */
class Version20161224104329 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('CREATE SEQUENCE inglese_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
            $this->addSql('CREATE TABLE inglese (id INT NOT NULL, class_computer VARCHAR(1) DEFAULT NULL, class_in VARCHAR(1) DEFAULT NULL, class_quickly VARCHAR(1) DEFAULT NULL, class_wonderful VARCHAR(1) DEFAULT NULL, class_went VARCHAR(1) DEFAULT NULL, class_on VARCHAR(1) DEFAULT NULL, class_enjoy VARCHAR(1) DEFAULT NULL, class_poor VARCHAR(1) DEFAULT NULL, class_factory VARCHAR(1) DEFAULT NULL, class_usually VARCHAR(1) DEFAULT NULL, class_came VARCHAR(1) DEFAULT NULL, best_option1 VARCHAR(1) DEFAULT NULL, best_option2 VARCHAR(1) DEFAULT NULL, best_option3 VARCHAR(1) DEFAULT NULL, best_option4 VARCHAR(1) DEFAULT NULL, best_option5 VARCHAR(1) DEFAULT NULL, best_option0 VARCHAR(1) DEFAULT NULL, pair1 VARCHAR(1) DEFAULT NULL, pair2 VARCHAR(1) DEFAULT NULL, pair3 VARCHAR(1) DEFAULT NULL, pair4 VARCHAR(1) DEFAULT NULL, pair5 VARCHAR(1) DEFAULT NULL, pair0 VARCHAR(1) DEFAULT NULL, pair_text0 VARCHAR(20) DEFAULT NULL, pair_text1 VARCHAR(20) DEFAULT NULL, pair_text2 VARCHAR(20) DEFAULT NULL, pair_text3 VARCHAR(20) DEFAULT NULL, pair_text4 VARCHAR(20) DEFAULT NULL, pair_text5 VARCHAR(20) DEFAULT NULL, sentence1 VARCHAR(1) DEFAULT NULL, sentence2 VARCHAR(1) DEFAULT NULL, user_id INT NOT NULL, mark INT DEFAULT NULL, start_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, sent BOOLEAN NOT NULL, marks TEXT DEFAULT NULL, PRIMARY KEY(id))');
            $this->addSql('COMMENT ON COLUMN inglese.marks IS \'(DC2Type:array)\'');
        }
        else{
            $this->addSql('CREATE TABLE inglese (id INT AUTO_INCREMENT NOT NULL, class_computer VARCHAR(1) DEFAULT NULL, class_in VARCHAR(1) DEFAULT NULL, class_quickly VARCHAR(1) DEFAULT NULL, class_wonderful VARCHAR(1) DEFAULT NULL, class_went VARCHAR(1) DEFAULT NULL, class_on VARCHAR(1) DEFAULT NULL, class_enjoy VARCHAR(1) DEFAULT NULL, class_poor VARCHAR(1) DEFAULT NULL, class_factory VARCHAR(1) DEFAULT NULL, class_usually VARCHAR(1) DEFAULT NULL, class_came VARCHAR(1) DEFAULT NULL, best_option1 VARCHAR(1) DEFAULT NULL, best_option2 VARCHAR(1) DEFAULT NULL, best_option3 VARCHAR(1) DEFAULT NULL, best_option4 VARCHAR(1) DEFAULT NULL, best_option5 VARCHAR(1) DEFAULT NULL, best_option0 VARCHAR(1) DEFAULT NULL, pair1 VARCHAR(1) DEFAULT NULL, pair2 VARCHAR(1) DEFAULT NULL, pair3 VARCHAR(1) DEFAULT NULL, pair4 VARCHAR(1) DEFAULT NULL, pair5 VARCHAR(1) DEFAULT NULL, pair0 VARCHAR(1) DEFAULT NULL,  pair_text0 VARCHAR(20) DEFAULT NULL, pair_text1 VARCHAR(20) DEFAULT NULL, pair_text2 VARCHAR(20) DEFAULT NULL, pair_text3 VARCHAR(20) DEFAULT NULL, pair_text4 VARCHAR(20) DEFAULT NULL, pair_text5 VARCHAR(20) DEFAULT NULL, sentence1 VARCHAR(1) DEFAULT NULL, sentence2 VARCHAR(1) DEFAULT NULL, user_id INT NOT NULL, mark INT DEFAULT NULL, start_time DATETIME NOT NULL, sent TINYINT(1) NOT NULL, marks LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('DROP SEQUENCE inglese_id_seq CASCADE');
        }
        $this->addSql('DROP TABLE inglese');
    }
}
