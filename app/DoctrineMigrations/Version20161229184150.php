<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * correct english sentences length
 */
class Version20161229184150 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('ALTER TABLE inglese ALTER sentence1 TYPE VARCHAR(200)');
            $this->addSql('ALTER TABLE inglese ALTER sentence2 TYPE VARCHAR(200)');
        }
        else $this->addSql('ALTER TABLE inglese CHANGE sentence1 sentence1 VARCHAR(200) DEFAULT NULL, CHANGE sentence2 sentence2 VARCHAR(200) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('ALTER TABLE inglese ALTER sentence1 TYPE VARCHAR(1)');
            $this->addSql('ALTER TABLE inglese ALTER sentence2 TYPE VARCHAR(1)');
        }
        else $this->addSql('ALTER TABLE inglese CHANGE sentence1 sentence1 VARCHAR(1) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE sentence2 sentence2 VARCHAR(1) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
