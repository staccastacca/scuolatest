<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Adds columns for second part of question 1 in italian test
 */
class Version20161128210548 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('ALTER TABLE italiano ADD due_punti_funzione_a VARCHAR(2) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD due_punti_funzione_b VARCHAR(2) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD due_punti_funzione_c VARCHAR(2) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD due_punti_funzione_d VARCHAR(2) DEFAULT NULL');
            $this->addSql('ALTER TABLE italiano ADD due_punti_funzione_e VARCHAR(2) DEFAULT NULL');
        }
        else{
            $this->addSql('ALTER TABLE italiano ADD due_punti_funzione_a VARCHAR(2) DEFAULT NULL, ADD due_punti_funzione_b VARCHAR(2) DEFAULT NULL, ADD due_punti_funzione_c VARCHAR(2) DEFAULT NULL, ADD due_punti_funzione_d VARCHAR(2) DEFAULT NULL, ADD due_punti_funzione_e VARCHAR(2) DEFAULT NULL');
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $dbPlatform = $this->connection->getDatabasePlatform()->getName();
        $this->abortIf($dbPlatform != 'postgresql' && $dbPlatform != 'mysql', 'Migration can only be executed safely on \'postgresql\' or \'mysql\'.');

        if($dbPlatform == 'postgresql'){
            $this->addSql('ALTER TABLE italiano DROP due_punti_funzione_a');
            $this->addSql('ALTER TABLE italiano DROP due_punti_funzione_b');
            $this->addSql('ALTER TABLE italiano DROP due_punti_funzione_c');
            $this->addSql('ALTER TABLE italiano DROP due_punti_funzione_d');
            $this->addSql('ALTER TABLE italiano DROP due_punti_funzione_e');
        }
        else{
            $this->addSql('ALTER TABLE italiano DROP due_punti_funzione_a, DROP due_punti_funzione_b, DROP due_punti_funzione_c, DROP due_punti_funzione_d, DROP due_punti_funzione_e');
        }
    }
}
