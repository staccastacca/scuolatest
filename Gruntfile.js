module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        bowercopy: {
            options: {
                srcPrefix: 'bower_components',
                destPrefix: 'web/assets'
            },
            scripts: {
                files: {
                    'js/jquery.min.js': 'jquery/dist/jquery.min.js',
                    'js/materialize.min.js': 'materialize/dist/js/materialize.min.js',
                    'js/chart.min.js': 'chart.js/dist/Chart.min.js',
                }
            },
            stylesheets: {
                files: {
                    'css/materialize.css': 'materialize/dist/css/materialize.min.css',
                }
            },
            fonts: {
                files: {
                    'fonts': 'materialize/fonts'
                }
            }
        },
        uglify: {
            scuolatest: {
                options: {
                    sourceMap: true,
                    sourceMapName: 'sourceMap.map'
                },
                files : {
                    'web/assets/js/common.min.js': [
                        'src/AppBundle/Resources/public/js/common.js',
                    ],
                    'web/assets/js/testForm.min.js': [
                        'src/AppBundle/Resources/public/js/testForm.js',
                    ],
                    'web/assets/js/stats.min.js': [
                        'src/AppBundle/Resources/public/js/stats.js',
                    ],
                }
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'web/assets/css/common.css': [
                        'src/AppBundle/Resources/public/css/common.css',
                    ],
                    'web/assets/css/baseuser.css': [
                        'src/AppBundle/Resources/public/css/baseuser.css',
                    ],
                    'web/assets/css/profile.css': [
                        'src/AppBundle/Resources/public/css/profile.css',
                    ],
                }
            }
        },
        imagemin: {
            scuolatest: {
                options: {
                    optimizationLevel: 3,
                },
                files: [
                    {
                        expand: true,
                        cwd: 'src/AppBundle/Resources/public/images/',
                        src: ['**/*.{png,jpg,gif}'],
                        dest: 'web/assets/images'
                    },
                ]
            }
        },
        watch: {
            scripts: {
                files: ['src/AppBundle/Resources/public/js/*.js'],
                tasks: ['uglify:scuolatest'],
                options: {
                    spawn: false,
                },
            },
            stylesheets: {
                files: ['src/AppBundle/Resources/public/css/*.css'],
                tasks: ['cssmin'],
                options: {
                    spawn: false,
                },
            },
            images: {
                files: ['src/AppBundle/Resources/public/images/*.{png,jpg,gif}'],
                tasks: ['imagemin:scuolatest'],
                options: {
                    spawn: false,
                },
            },
        },
    });

    grunt.loadNpmTasks('grunt-bowercopy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['bowercopy', 'uglify:scuolatest', 'cssmin', 'imagemin:scuolatest']);
};
