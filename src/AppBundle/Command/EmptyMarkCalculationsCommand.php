<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Controller\DefaultController;

class EmptyMarkCalculationsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('tests:empty-marks')
            ->setDescription('Drops all the mark calculations done forcing a recalculation.')
            ->setHelp('Drops all the mark calculations done forcing a recalculation.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $testTypes = DefaultController::getTestClasses();
        foreach($testTypes as $testType){
            $output->writeln('Clearing tests of type '.$testType::TESTNAME);
            $testRepo = $em->getRepository($testType::TESTREPOSITORY);
            $tests = $testRepo->findAll();
            foreach($tests as $test){
                $test->setMark(NULL);
                $test->setMarks(array());
            }
            $em->flush(); //DON'T YOU FORGET ABOUT MEEEEEEEE
        }
        $output->writeln('Mark calculations cleared.');
    }
}
