<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Test;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="italiano")
 */
class TestIta extends Test
{
    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $duePuntiA;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $duePuntiB;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $duePuntiC;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $duePuntiD;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $duePuntiE;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    protected $duePuntiFunzioneA;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    protected $duePuntiFunzioneB;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    protected $duePuntiFunzioneC;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    protected $duePuntiFunzioneD;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    protected $duePuntiFunzioneE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $punteggiaturaA;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $punteggiaturaB;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $virgolaA;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $virgolaB;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $virgolaC;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lessicoA;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lessicoB;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lessicoC;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lessicoD;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lessicoE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lessicoF;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lessicoG;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lessicoH;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lessicoI;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lessicoJ;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $pluraleGuerra;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $pluraleCapo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $pluraleCane;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $pluraleBuono;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $funzioneGA;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $funzioneGB;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $funzioneGC;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $funzioneLA;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $funzioneLB;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $funzioneLC;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $funzioneSA;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $funzioneSB;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $funzioneSC;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $argomentoTesto;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $dolcemente;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $nonCade;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $sequenza;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $tempo;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $spazio;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $narratore;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $buck;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $lessico;

    /**
    * Set lessicoA
    *
    * @param string $lessicoA
    *
    * @return TestFisica
    */
    public function setLessicoA($lessicoA)
    {
    $this->lessicoA = $lessicoA;

    return $this;
    }

    /**
    * Get lessicoA
    *
    * @return string
    */
    public function getLessicoA()
    {
    return $this->lessicoA;
    }

    /**
    * Set lessicoB
    *
    * @param string $lessicoB
    *
    * @return TestFisica
    */
    public function setLessicoB($lessicoB)
    {
    $this->lessicoB = $lessicoB;

    return $this;
    }

    /**
    * Get lessicoB
    *
    * @return string
    */
    public function getLessicoB()
    {
    return $this->lessicoB;
    }

    /**
    * Set lessicoC
    *
    * @param string $lessicoC
    *
    * @return TestFisica
    */
    public function setLessicoC($lessicoC)
    {
    $this->lessicoC = $lessicoC;

    return $this;
    }

    /**
    * Get lessicoC
    *
    * @return string
    */
    public function getLessicoC()
    {
    return $this->lessicoC;
    }

    /**
    * Set lessicoD
    *
    * @param string $lessicoD
    *
    * @return TestFisica
    */
    public function setLessicoD($lessicoD)
    {
    $this->lessicoD = $lessicoD;

    return $this;
    }

    /**
    * Get lessicoD
    *
    * @return string
    */
    public function getLessicoD()
    {
    return $this->lessicoD;
    }

    /**
    * Set lessicoE
    *
    * @param string $lessicoE
    *
    * @return TestFisica
    */
    public function setLessicoE($lessicoE)
    {
    $this->lessicoE = $lessicoE;

    return $this;
    }

    /**
    * Get lessicoE
    *
    * @return string
    */
    public function getLessicoE()
    {
    return $this->lessicoE;
    }

    /**
    * Set lessicoF
    *
    * @param string $lessicoF
    *
    * @return TestFisica
    */
    public function setLessicoF($lessicoF)
    {
    $this->lessicoF = $lessicoF;

    return $this;
    }

    /**
    * Get lessicoF
    *
    * @return string
    */
    public function getLessicoF()
    {
    return $this->lessicoF;
    }

    /**
    * Set lessicoG
    *
    * @param string $lessicoG
    *
    * @return TestFisica
    */
    public function setLessicoG($lessicoG)
    {
    $this->lessicoG = $lessicoG;

    return $this;
    }

    /**
    * Get lessicoG
    *
    * @return string
    */
    public function getLessicoG()
    {
    return $this->lessicoG;
    }

    /**
    * Set lessicoH
    *
    * @param string $lessicoH
    *
    * @return TestFisica
    */
    public function setLessicoH($lessicoH)
    {
    $this->lessicoH = $lessicoH;

    return $this;
    }

    /**
    * Get lessicoH
    *
    * @return string
    */
    public function getLessicoH()
    {
    return $this->lessicoH;
    }

    /**
    * Set lessicoI
    *
    * @param string $lessicoI
    *
    * @return TestFisica
    */
    public function setLessicoI($lessicoI)
    {
    $this->lessicoI = $lessicoI;

    return $this;
    }
    /**
    * Get lessicoI
    *
    * @return string
    */
    public function getLessicoI()
    {
      return $this->lessicoI;
    }

    /**
    * Set lessicoJ
    *
    * @param string $lessicoJ
    *
    * @return TestFisica
    */
    public function setLessicoJ($lessicoJ)
    {
      $this->lessicoJ = $lessicoJ;

      return $this;
    }

    /**
    * Get lessicoJ
    *
    * @return string
    */
    public function getLessicoJ()
    {
      return $this->lessicoJ;
    }

    /**
     * Set duePuntiA
     *
     * @param string $duePuntiA
     *
     * @return TestIta
     */
    public function setDuePuntiA($duePuntiA)
    {
        $this->duePuntiA = $duePuntiA;

        return $this;
    }

    /**
     * Get duePuntiA
     *
     * @return string
     */
    public function getDuePuntiA()
    {
        return $this->duePuntiA;
    }

    /**
     * Set duePuntiB
     *
     * @param string $duePuntiB
     *
     * @return TestIta
     */
    public function setDuePuntiB($duePuntiB)
    {
        $this->duePuntiB = $duePuntiB;

        return $this;
    }

    /**
     * Get duePuntiB
     *
     * @return string
     */
    public function getDuePuntiB()
    {
        return $this->duePuntiB;
    }

    /**
     * Set duePuntiC
     *
     * @param string $duePuntiC
     *
     * @return TestIta
     */
    public function setDuePuntiC($duePuntiC)
    {
        $this->duePuntiC = $duePuntiC;

        return $this;
    }

    /**
     * Get duePuntiC
     *
     * @return string
     */
    public function getDuePuntiC()
    {
        return $this->duePuntiC;
    }

    /**
     * Set duePuntiD
     *
     * @param string $duePuntiD
     *
     * @return TestIta
     */
    public function setDuePuntiD($duePuntiD)
    {
        $this->duePuntiD = $duePuntiD;

        return $this;
    }

    /**
     * Get duePuntiD
     *
     * @return string
     */
    public function getDuePuntiD()
    {
        return $this->duePuntiD;
    }

    /**
     * Set duePuntiE
     *
     * @param string $duePuntiE
     *
     * @return TestIta
     */
    public function setDuePuntiE($duePuntiE)
    {
        $this->duePuntiE = $duePuntiE;

        return $this;
    }

    /**
     * Get duePuntiE
     *
     * @return string
     */
    public function getDuePuntiE()
    {
        return $this->duePuntiE;
    }

    /**
     * Set pluraleGuerra
     *
     * @param string $pluraleGuerra
     *
     * @return TestIta
     */
    public function setPluraleGuerra($pluraleGuerra)
    {
        $this->pluraleGuerra = $pluraleGuerra;

        return $this;
    }

    /**
     * Get pluraleGuerra
     *
     * @return string
     */
    public function getPluraleGuerra()
    {
        return $this->pluraleGuerra;
    }

    /**
     * Set pluraleCapo
     *
     * @param string $pluraleCapo
     *
     * @return TestIta
     */
    public function setPluraleCapo($pluraleCapo)
    {
        $this->pluraleCapo = $pluraleCapo;

        return $this;
    }

    /**
     * Get pluraleCapo
     *
     * @return string
     */
    public function getPluraleCapo()
    {
        return $this->pluraleCapo;
    }

    /**
     * Set pluralecane
     *
     * @param string $pluralecane
     *
     * @return TestIta
     */
    public function setPluraleCane($pluralecane)
    {
        $this->pluraleCane = $pluralecane;

        return $this;
    }

    /**
     * Get pluralecane
     *
     * @return string
     */
    public function getPluraleCane()
    {
        return $this->pluraleCane;
    }

    /**
     * Set pluraleBuono
     *
     * @param string $pluraleBuono
     *
     * @return TestIta
     */
    public function setPluraleBuono($pluraleBuono)
    {
        $this->pluraleBuono = $pluraleBuono;

        return $this;
    }

    /**
     * Get pluraleBuono
     *
     * @return string
     */
    public function getPluraleBuono()
    {
        return $this->pluraleBuono;
    }

    /**
     * Set funzioneGA
     *
     * @param string $funzioneGA
     *
     * @return TestIta
     */
    public function setFunzioneGA($funzioneGA)
    {
        $this->funzioneGA = $funzioneGA;

        return $this;
    }

    /**
     * Get funzioneGA
     *
     * @return string
     */
    public function getFunzioneGA()
    {
        return $this->funzioneGA;
    }

    /**
     * Set funzioneGB
     *
     * @param string $funzioneGB
     *
     * @return TestIta
     */
    public function setFunzioneGB($funzioneGB)
    {
        $this->funzioneGB = $funzioneGB;

        return $this;
    }

    /**
     * Get funzioneGB
     *
     * @return string
     */
    public function getFunzioneGB()
    {
        return $this->funzioneGB;
    }

    /**
     * Set funzioneGC
     *
     * @param string $funzioneGC
     *
     * @return TestIta
     */
    public function setFunzioneGC($funzioneGC)
    {
        $this->funzioneGC = $funzioneGC;

        return $this;
    }

    /**
     * Get funzioneGC
     *
     * @return string
     */
    public function getFunzioneGC()
    {
        return $this->funzioneGC;
    }

    /**
     * Set funzioneLA
     *
     * @param string $funzioneLA
     *
     * @return TestIta
     */
    public function setFunzioneLA($funzioneLA)
    {
        $this->funzioneLA = $funzioneLA;

        return $this;
    }

    /**
     * Get funzioneLA
     *
     * @return string
     */
    public function getFunzioneLA()
    {
        return $this->funzioneLA;
    }

    /**
     * Set funzioneLB
     *
     * @param string $funzioneLB
     *
     * @return TestIta
     */
    public function setFunzioneLB($funzioneLB)
    {
        $this->funzioneLB = $funzioneLB;

        return $this;
    }

    /**
     * Get funzioneLB
     *
     * @return string
     */
    public function getFunzioneLB()
    {
        return $this->funzioneLB;
    }

    /**
     * Set funzioneLC
     *
     * @param string $funzioneLC
     *
     * @return TestIta
     */
    public function setFunzioneLC($funzioneLC)
    {
        $this->funzioneLC = $funzioneLC;

        return $this;
    }

    /**
     * Get funzioneLC
     *
     * @return string
     */
    public function getFunzioneLC()
    {
        return $this->funzioneLC;
    }

    /**
     * Set funzioneSA
     *
     * @param string $funzioneSA
     *
     * @return TestIta
     */
    public function setFunzioneSA($funzioneSA)
    {
        $this->funzioneSA = $funzioneSA;

        return $this;
    }

    /**
     * Get funzioneSA
     *
     * @return string
     */
    public function getFunzioneSA()
    {
        return $this->funzioneSA;
    }

    /**
     * Set funzioneSB
     *
     * @param string $funzioneSB
     *
     * @return TestIta
     */
    public function setFunzioneSB($funzioneSB)
    {
        $this->funzioneSB = $funzioneSB;

        return $this;
    }

    /**
     * Get funzioneSB
     *
     * @return string
     */
    public function getFunzioneSB()
    {
        return $this->funzioneSB;
    }

    /**
     * Set funzioneSC
     *
     * @param string $funzioneSC
     *
     * @return TestIta
     */
    public function setFunzioneSC($funzioneSC)
    {
        $this->funzioneSC = $funzioneSC;

        return $this;
    }

    /**
     * Get funzioneSC
     *
     * @return string
     */
    public function getFunzioneSC()
    {
        return $this->funzioneSC;
    }

    /**
     * Set argomentoTesto
     *
     * @param string $argomentoTesto
     *
     * @return TestIta
     */
    public function setArgomentoTesto($argomentoTesto)
    {
        $this->argomentoTesto = $argomentoTesto;

        return $this;
    }

    /**
     * Get argomentoTesto
     *
     * @return string
     */
    public function getArgomentoTesto()
    {
        return $this->argomentoTesto;
    }

    /**
     * Set dolcemente
     *
     * @param string $dolcemente
     *
     * @return TestIta
     */
    public function setDolcemente($dolcemente)
    {
        $this->dolcemente = $dolcemente;

        return $this;
    }

    /**
     * Get dolcemente
     *
     * @return string
     */
    public function getDolcemente()
    {
        return $this->dolcemente;
    }

    /**
     * Set nonCade
     *
     * @param string $nonCade
     *
     * @return TestIta
     */
    public function setNonCade($nonCade)
    {
        $this->nonCade = $nonCade;

        return $this;
    }

    /**
     * Get nonCade
     *
     * @return string
     */
    public function getNonCade()
    {
        return $this->nonCade;
    }

    /**
     * Set sequenza
     *
     * @param string $sequenza
     *
     * @return TestIta
     */
    public function setSequenza($sequenza)
    {
        $this->sequenza = $sequenza;

        return $this;
    }

    /**
     * Get sequenza
     *
     * @return string
     */
    public function getSequenza()
    {
        return $this->sequenza;
    }

    /**
     * Set tempo
     *
     * @param string $tempo
     *
     * @return TestIta
     */
    public function setTempo($tempo)
    {
        $this->tempo = $tempo;

        return $this;
    }

    /**
     * Get tempo
     *
     * @return string
     */
    public function getTempo()
    {
        return $this->tempo;
    }

    /**
     * Set spazio
     *
     * @param string $spazio
     *
     * @return TestIta
     */
    public function setSpazio($spazio)
    {
        $this->spazio = $spazio;

        return $this;
    }

    /**
     * Get spazio
     *
     * @return string
     */
    public function getSpazio()
    {
        return $this->spazio;
    }

    /**
     * Set narratore
     *
     * @param string $narratore
     *
     * @return TestIta
     */
    public function setNarratore($narratore)
    {
        $this->narratore = $narratore;

        return $this;
    }

    /**
     * Get narratore
     *
     * @return string
     */
    public function getNarratore()
    {
        return $this->narratore;
    }

    /**
     * Set buck
     *
     * @param string $buck
     *
     * @return TestIta
     */
    public function setBuck($buck)
    {
        $this->buck = $buck;

        return $this;
    }

    /**
     * Get buck
     *
     * @return string
     */
    public function getBuck()
    {
        return $this->buck;
    }

    /**
     * Set lessico
     *
     * @param string $lessico
     *
     * @return TestIta
     */
    public function setLessico($lessico)
    {
        $this->lessico = $lessico;

        return $this;
    }

    /**
     * Get lessico
     *
     * @return string
     */
    public function getLessico()
    {
        return $this->lessico;
    }

    /**
     * Set duePuntiFunzioneA
     *
     * @param string $duePuntiFunzioneA
     *
     * @return TestIta
     */
    public function setDuePuntiFunzioneA($duePuntiFunzioneA)
    {
        $this->duePuntiFunzioneA = $duePuntiFunzioneA;

        return $this;
    }

    /**
     * Get duePuntiFunzioneA
     *
     * @return string
     */
    public function getDuePuntiFunzioneA()
    {
        return $this->duePuntiFunzioneA;
    }

    /**
     * Set duePuntiFunzioneB
     *
     * @param string $duePuntiFunzioneB
     *
     * @return TestIta
     */
    public function setDuePuntiFunzioneB($duePuntiFunzioneB)
    {
        $this->duePuntiFunzioneB = $duePuntiFunzioneB;

        return $this;
    }

    /**
     * Get duePuntiFunzioneB
     *
     * @return string
     */
    public function getDuePuntiFunzioneB()
    {
        return $this->duePuntiFunzioneB;
    }

    /**
     * Set duePuntiFunzioneC
     *
     * @param string $duePuntiFunzioneC
     *
     * @return TestIta
     */
    public function setDuePuntiFunzioneC($duePuntiFunzioneC)
    {
        $this->duePuntiFunzioneC = $duePuntiFunzioneC;

        return $this;
    }

    /**
     * Get duePuntiFunzioneC
     *
     * @return string
     */
    public function getDuePuntiFunzioneC()
    {
        return $this->duePuntiFunzioneC;
    }

    /**
     * Set duePuntiFunzioneD
     *
     * @param string $duePuntiFunzioneD
     *
     * @return TestIta
     */
    public function setDuePuntiFunzioneD($duePuntiFunzioneD)
    {
        $this->duePuntiFunzioneD = $duePuntiFunzioneD;

        return $this;
    }

    /**
     * Get duePuntiFunzioneD
     *
     * @return string
     */
    public function getDuePuntiFunzioneD()
    {
        return $this->duePuntiFunzioneD;
    }

    /**
     * Set duePuntiFunzioneE
     *
     * @param string $duePuntiFunzioneE
     *
     * @return TestIta
     */
    public function setDuePuntiFunzioneE($duePuntiFunzioneE)
    {
        $this->duePuntiFunzioneE = $duePuntiFunzioneE;

        return $this;
    }

    /**
     * Get duePuntiFunzioneE
     *
     * @return string
     */
    public function getDuePuntiFunzioneE()
    {
        return $this->duePuntiFunzioneE;
    }

    /**
     * Set punteggiaturaA
     *
     * @param string $punteggiaturaA
     *
     * @return TestIta
     */
    public function setPunteggiaturaA($punteggiaturaA)
    {
        $this->punteggiaturaA = $punteggiaturaA;

        return $this;
    }

    /**
     * Get punteggiaturaA
     *
     * @return string
     */
    public function getPunteggiaturaA()
    {
        return $this->punteggiaturaA;
    }

    /**
     * Set punteggiaturaB
     *
     * @param string $punteggiaturaB
     *
     * @return TestIta
     */
    public function setPunteggiaturaB($punteggiaturaB)
    {
        $this->punteggiaturaB = $punteggiaturaB;

        return $this;
    }

    /**
     * Get punteggiaturaB
     *
     * @return string
     */
    public function getPunteggiaturaB()
    {
        return $this->punteggiaturaB;
    }

    /**
     * Set virgolaA
     *
     * @param string $virgolaA
     *
     * @return TestIta
     */
    public function setVirgolaA($virgolaA)
    {
        $this->virgolaA = $virgolaA;

        return $this;
    }

    /**
     * Get virgolaA
     *
     * @return string
     */
    public function getVirgolaA()
    {
        return $this->virgolaA;
    }

    /**
     * Set virgolaB
     *
     * @param string $virgolaB
     *
     * @return TestIta
     */
    public function setVirgolaB($virgolaB)
    {
        $this->virgolaB = $virgolaB;

        return $this;
    }

    /**
     * Get virgolaB
     *
     * @return string
     */
    public function getVirgolaB()
    {
        return $this->virgolaB;
    }

    /**
     * Set virgolaC
     *
     * @param string $virgolaC
     *
     * @return TestIta
     */
    public function setVirgolaC($virgolaC)
    {
        $this->virgolaC = $virgolaC;

        return $this;
    }

    /**
     * Get virgolaC
     *
     * @return string
     */
    public function getVirgolaC()
    {
        return $this->virgolaC;
    }
}
