<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\OneToOne(targetEntity="Test", mappedBy="userId")
     * @ORM\OneToOne(targetEntity="Feedback", mappedBy="userId")
     */
    protected $id;

    /**
     * Override of username variable in BaseUser
     * @Assert\Regex(
     *     pattern = "/[\s@]/",
     *     match=false,
     *     message = "'@' e spazi bianchi non sono ammessi nel nome utente"
     * )
     */
    protected $username;

    /**
     * If the email is checked
     * @ORM\Column(type="boolean")
     */
    protected $email_checked=false;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Set cardNumber
     *
     * @param integer $cardNumber
     *
     * @return User
     */
    public function setCardNumber($cardNumber)
    {
        $this->card_number = $cardNumber;

        return $this;
    }

    /**
     * Get cardNumber
     *
     * @return integer
     */
    public function getCardNumber()
    {
        return $this->card_number;
    }

    /**
     * Get expiresAt
     *
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Get credentials_expire_at
     *
     * @return \DateTime
     */
    public function getCredentialsExpireAt()
    {
        return $this->credentialsExpireAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set emailChecked
     *
     * @param boolean $emailChecked
     *
     * @return User
     */
    public function setEmailChecked($emailChecked)
    {
        $this->email_checked = $emailChecked;

        return $this;
    }

    /**
     * Get emailChecked
     *
     * @return boolean
     */
    public function getEmailChecked()
    {
        return $this->email_checked;
    }
}
