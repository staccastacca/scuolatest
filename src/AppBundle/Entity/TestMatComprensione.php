<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Test;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="comprensione")
 */
class TestMatComprensione extends Test
{
    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $sydneyBerlino;

    /**
    * @ORM\Column(type="time", nullable=true)
    */
    protected $oraSydney;

    /**
    * @ORM\Column(type="time", nullable=true)
    */
    protected $oraBerlino;

    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    protected $moltiplicazioneIndiana;

    /**
     * Set moltiplicazioneIndiana
     *
     * @param string $moltiplicazioneIndiana
     *
     * @return TestMatematica
     */
    public function setMoltiplicazioneIndiana($moltiplicazioneIndiana)
    {
        $this->moltiplicazioneIndiana = $moltiplicazioneIndiana;

        return $this;
    }

    /**
     * Get moltiplicazioneIndiana
     *
     * @return string
     */
    public function getMoltiplicazioneIndiana()
    {
        return $this->moltiplicazioneIndiana;
    }

    /**
     * Set sydneyBerlino
     *
     * @param string $sydneyBerlino
     *
     * @return Test
     */
    public function setSydneyBerlino($sydneyBerlino)
    {
        $this->sydneyBerlino = $sydneyBerlino;

        return $this;
    }

    /**
     * Get sydneyBerlino
     *
     * @return string
     */
    public function getSydneyBerlino()
    {
        return $this->sydneyBerlino;
    }

    /**
     * Set oraSydney
     *
     * @param \DateTime $oraSydney
     *
     * @return Test
     */
    public function setOraSydney($oraSydney)
    {
        $this->oraSydney = $oraSydney;

        return $this;
    }

    /**
     * Get oraSydney
     *
     * @return \DateTime
     */
    public function getOraSydney()
    {
        return $this->oraSydney;
    }

    /**
     * Set oraBerlino
     *
     * @param \DateTime $oraBerlino
     *
     * @return Test
     */
    public function setOraBerlino($oraBerlino)
    {
        $this->oraBerlino = $oraBerlino;

        return $this;
    }

    /**
     * Get oraBerlino
     *
     * @return \DateTime
     */
    public function getOraBerlino()
    {
        return $this->oraBerlino;
    }
}
