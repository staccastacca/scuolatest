<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Test;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="matematica")
 */
class TestMatematica extends Test
{
    /**
     * esercizi geometria
     */

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $angoli;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $rombo;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $foglioCarta;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $semicerchio;

    /**
     * vero o falso di geometria
     */

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $veroFalsoGeometria1;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $veroFalsoGeometria2;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $veroFalsoGeometria3;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $veroFalsoGeometria4;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $relazioni;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $distanzaPunti;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $triangoli;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $simmetria;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $circonferenza;

    /**
     * algebra
     */

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $fattoriPrimi;

    /**
    * @ORM\Column(type="boolean", nullable=true)
    */
    protected $MCDormcm1;

    /**
    * @ORM\Column(type="boolean", nullable=true)
    */
    protected $MCDormcm2;

    /**
    * @ORM\Column(type="boolean", nullable=true)
    */
    protected $MCDormcm3;

    /**
    * @ORM\Column(type="boolean", nullable=true)
    */
    protected $MCDormcm4;

    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    protected $tabNum1;

    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    protected $tabNum2;

    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    protected $tabNum3;


    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    protected $tabNum4;


    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    protected $tabNum5;


    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    protected $tabNum6;

    /**
    * @ORM\Column(type="boolean", nullable=true)
    */
    protected $siNoPrimi;

    /**
    * @ORM\Column(type="boolean", nullable=true)
    */
    protected $siNoPrimiLoro;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $unMezzo;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $numeroRazionale;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $medicina;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $cannavacciuolo;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $equazione;

    /**
    * @ORM\Column(type="string", length=1, nullable=true)
    */
    protected $successivo;

    /**
     * Set angoli
     *
     * @param string $angoli
     *
     * @return Test
     */
    public function setAngoli($angoli)
    {
        $this->angoli = $angoli;

        return $this;
    }

    /**
     * Get angoli
     *
     * @return string
     */
    public function getAngoli()
    {
        return $this->angoli;
    }

    /**
     * Set rombo
     *
     * @param string $rombo
     *
     * @return Test
     */
    public function setRombo($rombo)
    {
        $this->rombo = $rombo;

        return $this;
    }

    /**
     * Get rombo
     *
     * @return string
     */
    public function getRombo()
    {
        return $this->rombo;
    }

    /**
     * Set foglioCarta
     *
     * @param string $foglioCarta
     *
     * @return Test
     */
    public function setFoglioCarta($foglioCarta)
    {
        $this->foglioCarta = $foglioCarta;

        return $this;
    }

    /**
     * Get foglioCarta
     *
     * @return string
     */
    public function getFoglioCarta()
    {
        return $this->foglioCarta;
    }

    /**
     * Set semicerchio
     *
     * @param string $semicerchio
     *
     * @return Test
     */
    public function setSemicerchio($semicerchio)
    {
        $this->semicerchio = $semicerchio;

        return $this;
    }

    /**
     * Get semicerchio
     *
     * @return string
     */
    public function getSemicerchio()
    {
        return $this->semicerchio;
    }

    /**
     * Set veroFalsoGeometria1
     *
     * @param boolean $veroFalsoGeometria1
     *
     * @return Test
     */
    public function setVeroFalsoGeometria1($veroFalsoGeometria1)
    {
        $this->veroFalsoGeometria1 = $veroFalsoGeometria1;

        return $this;
    }

    /**
     * Get veroFalsoGeometria1
     *
     * @return boolean
     */
    public function getVeroFalsoGeometria1()
    {
        return $this->veroFalsoGeometria1;
    }

    /**
     * Set veroFalsoGeometria2
     *
     * @param boolean $veroFalsoGeometria2
     *
     * @return Test
     */
    public function setVeroFalsoGeometria2($veroFalsoGeometria2)
    {
        $this->veroFalsoGeometria2 = $veroFalsoGeometria2;

        return $this;
    }

    /**
     * Get veroFalsoGeometria2
     *
     * @return boolean
     */
    public function getVeroFalsoGeometria2()
    {
        return $this->veroFalsoGeometria2;
    }

    /**
     * Set veroFalsoGeometria3
     *
     * @param boolean $veroFalsoGeometria3
     *
     * @return Test
     */
    public function setVeroFalsoGeometria3($veroFalsoGeometria3)
    {
        $this->veroFalsoGeometria3 = $veroFalsoGeometria3;

        return $this;
    }

    /**
     * Get veroFalsoGeometria3
     *
     * @return boolean
     */
    public function getVeroFalsoGeometria3()
    {
        return $this->veroFalsoGeometria3;
    }

    /**
     * Set veroFalsoGeometria4
     *
     * @param boolean $veroFalsoGeometria4
     *
     * @return Test
     */
    public function setVeroFalsoGeometria4($veroFalsoGeometria4)
    {
        $this->veroFalsoGeometria4 = $veroFalsoGeometria4;

        return $this;
    }

    /**
     * Get veroFalsoGeometria4
     *
     * @return boolean
     */
    public function getVeroFalsoGeometria4()
    {
        return $this->veroFalsoGeometria4;
    }

    /**
     * Set relazioni
     *
     * @param string $relazioni
     *
     * @return Test
     */
    public function setRelazioni($relazioni)
    {
        $this->relazioni = $relazioni;

        return $this;
    }

    /**
     * Get relazioni
     *
     * @return string
     */
    public function getRelazioni()
    {
        return $this->relazioni;
    }

    /**
     * Set distanzaPunti
     *
     * @param string $distanzaPunti
     *
     * @return Test
     */
    public function setDistanzaPunti($distanzaPunti)
    {
        $this->distanzaPunti = $distanzaPunti;

        return $this;
    }

    /**
     * Get distanzaPunti
     *
     * @return string
     */
    public function getDistanzaPunti()
    {
        return $this->distanzaPunti;
    }

    /**
     * Set triangoli
     *
     * @param string $triangoli
     *
     * @return Test
     */
    public function setTriangoli($triangoli)
    {
        $this->triangoli = $triangoli;

        return $this;
    }

    /**
     * Get triangoli
     *
     * @return string
     */
    public function getTriangoli()
    {
        return $this->triangoli;
    }

    /**
     * Set simmetria
     *
     * @param string $simmetria
     *
     * @return Test
     */
    public function setSimmetria($simmetria)
    {
        $this->simmetria = $simmetria;

        return $this;
    }

    /**
     * Get simmetria
     *
     * @return string
     */
    public function getSimmetria()
    {
        return $this->simmetria;
    }

    /**
     * Set circonferenza
     *
     * @param string $circonferenza
     *
     * @return Test
     */
    public function setCirconferenza($circonferenza)
    {
        $this->circonferenza = $circonferenza;

        return $this;
    }

    /**
     * Get circonferenza
     *
     * @return string
     */
    public function getCirconferenza()
    {
        return $this->circonferenza;
    }

    /**
     * Set fattoriPrimi
     *
     * @param string $fattoriPrimi
     *
     * @return Test
     */
    public function setFattoriPrimi($fattoriPrimi)
    {
        $this->fattoriPrimi = $fattoriPrimi;

        return $this;
    }

    /**
     * Get fattoriPrimi
     *
     * @return string
     */
    public function getFattoriPrimi()
    {
        return $this->fattoriPrimi;
    }

    /**
     * Set mCDormcm
     *
     * @param string $mCDormcm
     *
     * @return Test
     */
    public function setMCDormcm($mCDormcm)
    {
        $this->MCDormcm = $mCDormcm;

        return $this;
    }

    /**
     * Get mCDormcm
     *
     * @return string
     */
    public function getMCDormcm()
    {
        return $this->MCDormcm;
    }

    /**
     * Set tabNum1
     *
     * @param string $tabNum1
     *
     * @return Test
     */
    public function setTabNum1($tabNum1)
    {
        $this->tabNum1 = $tabNum1;

        return $this;
    }

    /**
     * Get tabNum1
     *
     * @return string
     */
    public function getTabNum1()
    {
        return $this->tabNum1;
    }

    /**
     * Set tabNum2
     *
     * @param string $tabNum2
     *
     * @return Test
     */
    public function setTabNum2($tabNum2)
    {
        $this->tabNum2 = $tabNum2;

        return $this;
    }

    /**
     * Get tabNum2
     *
     * @return string
     */
    public function getTabNum2()
    {
        return $this->tabNum2;
    }

    /**
     * Set tabNum3
     *
     * @param string $tabNum3
     *
     * @return Test
     */
    public function setTabNum3($tabNum3)
    {
        $this->tabNum3 = $tabNum3;

        return $this;
    }

    /**
     * Get tabNum3
     *
     * @return string
     */
    public function getTabNum3()
    {
        return $this->tabNum3;
    }

    /**
     * Set tabNum4
     *
     * @param string $tabNum4
     *
     * @return Test
     */
    public function setTabNum4($tabNum4)
    {
        $this->tabNum4 = $tabNum4;

        return $this;
    }

    /**
     * Get tabNum4
     *
     * @return string
     */
    public function getTabNum4()
    {
        return $this->tabNum4;
    }

    /**
     * Set tabNum5
     *
     * @param string $tabNum5
     *
     * @return Test
     */
    public function setTabNum5($tabNum5)
    {
        $this->tabNum5 = $tabNum5;

        return $this;
    }

    /**
     * Get tabNum5
     *
     * @return string
     */
    public function getTabNum5()
    {
        return $this->tabNum5;
    }

    /**
     * Set tabNum6
     *
     * @param string $tabNum6
     *
     * @return Test
     */
    public function setTabNum6($tabNum6)
    {
        $this->tabNum6 = $tabNum6;

        return $this;
    }

    /**
     * Get tabNum6
     *
     * @return string
     */
    public function getTabNum6()
    {
        return $this->tabNum6;
    }

    /**
     * Set siNoPrimi
     *
     * @param boolean $siNoPrimi
     *
     * @return Test
     */
    public function setSiNoPrimi($siNoPrimi)
    {
        $this->siNoPrimi = $siNoPrimi;

        return $this;
    }

    /**
     * Get siNoPrimi
     *
     * @return boolean
     */
    public function getSiNoPrimi()
    {
        return $this->siNoPrimi;
    }

    /**
     * Set siNoPrimiLoro
     *
     * @param boolean $siNoPrimiLoro
     *
     * @return Test
     */
    public function setSiNoPrimiLoro($siNoPrimiLoro)
    {
        $this->siNoPrimiLoro = $siNoPrimiLoro;

        return $this;
    }

    /**
     * Get siNoPrimiLoro
     *
     * @return boolean
     */
    public function getSiNoPrimiLoro()
    {
        return $this->siNoPrimiLoro;
    }

    /**
     * Set unMezzo
     *
     * @param string $unMezzo
     *
     * @return Test
     */
    public function setUnMezzo($unMezzo)
    {
        $this->unMezzo = $unMezzo;

        return $this;
    }

    /**
     * Get unMezzo
     *
     * @return string
     */
    public function getUnMezzo()
    {
        return $this->unMezzo;
    }

    /**
     * Set numeroRazionale
     *
     * @param string $numeroRazionale
     *
     * @return Test
     */
    public function setNumeroRazionale($numeroRazionale)
    {
        $this->numeroRazionale = $numeroRazionale;

        return $this;
    }

    /**
     * Get numeroRazionale
     *
     * @return string
     */
    public function getNumeroRazionale()
    {
        return $this->numeroRazionale;
    }

    /**
     * Set medicina
     *
     * @param string $medicina
     *
     * @return Test
     */
    public function setMedicina($medicina)
    {
        $this->medicina = $medicina;

        return $this;
    }

    /**
     * Get medicina
     *
     * @return string
     */
    public function getMedicina()
    {
        return $this->medicina;
    }

    /**
     * Set cannavacciuolo
     *
     * @param string $cannavacciuolo
     *
     * @return Test
     */
    public function setCannavacciuolo($cannavacciuolo)
    {
        $this->cannavacciuolo = $cannavacciuolo;

        return $this;
    }

    /**
     * Get cannavacciuolo
     *
     * @return string
     */
    public function getCannavacciuolo()
    {
        return $this->cannavacciuolo;
    }

    /**
     * Set equazione
     *
     * @param string $equazione
     *
     * @return Test
     */
    public function setEquazione($equazione)
    {
        $this->equazione = $equazione;

        return $this;
    }

    /**
     * Get equazione
     *
     * @return string
     */
    public function getEquazione()
    {
        return $this->equazione;
    }

    /**
     * Set successivo
     *
     * @param string $successivo
     *
     * @return Test
     */
    public function setSuccessivo($successivo)
    {
        $this->successivo = $successivo;

        return $this;
    }

    /**
     * Get successivo
     *
     * @return string
     */
    public function getSuccessivo()
    {
        return $this->successivo;
    }

    /**
     * Set mCDormcm1
     *
     * @param string $mCDormcm1
     *
     * @return TestMatematica
     */
    public function setMCDormcm1($mCDormcm1)
    {
        $this->MCDormcm1 = $mCDormcm1;

        return $this;
    }

    /**
     * Get mCDormcm1
     *
     * @return string
     */
    public function getMCDormcm1()
    {
        return $this->MCDormcm1;
    }

    /**
     * Set mCDormcm2
     *
     * @param string $mCDormcm2
     *
     * @return TestMatematica
     */
    public function setMCDormcm2($mCDormcm2)
    {
        $this->MCDormcm2 = $mCDormcm2;

        return $this;
    }

    /**
     * Get mCDormcm2
     *
     * @return string
     */
    public function getMCDormcm2()
    {
        return $this->MCDormcm2;
    }

    /**
     * Set mCDormcm3
     *
     * @param string $mCDormcm3
     *
     * @return TestMatematica
     */
    public function setMCDormcm3($mCDormcm3)
    {
        $this->MCDormcm3 = $mCDormcm3;

        return $this;
    }

    /**
     * Get mCDormcm3
     *
     * @return string
     */
    public function getMCDormcm3()
    {
        return $this->MCDormcm3;
    }

    /**
     * Set mCDormcm4
     *
     * @param string $mCDormcm4
     *
     * @return TestMatematica
     */
    public function setMCDormcm4($mCDormcm4)
    {
        $this->MCDormcm4 = $mCDormcm4;

        return $this;
    }

    /**
     * Get mCDormcm4
     *
     * @return string
     */
    public function getMCDormcm4()
    {
        return $this->MCDormcm4;
    }
}
