<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Test;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="inglese")
 */
class TestInglese extends Test
{
    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $classComputer;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $classIn;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $classQuickly;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $classWonderful;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $classWent;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $classOn;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $classEnjoy;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $classPoor;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $classFactory;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $classUsually;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $classCame;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $bestOption1;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $bestOption2;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $bestOption3;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $bestOption4;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $bestOption5;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $bestOption0;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $pair1;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $pair2;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $pair3;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $pair4;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $pair5;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $pair0;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $pairText1;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $pairText2;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $pairText3;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $pairText4;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $pairText5;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $pairText0;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $sentence1;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $sentence2;

    /**
     * Set classComputer
     *
     * @param string $classComputer
     *
     * @return TestInglese
     */
    public function setClassComputer($classComputer)
    {
        $this->classComputer = $classComputer;

        return $this;
    }

    /**
     * Get classComputer
     *
     * @return string
     */
    public function getClassComputer()
    {
        return $this->classComputer;
    }

    /**
     * Set classIn
     *
     * @param string $classIn
     *
     * @return TestInglese
     */
    public function setClassIn($classIn)
    {
        $this->classIn = $classIn;

        return $this;
    }

    /**
     * Get classIn
     *
     * @return string
     */
    public function getClassIn()
    {
        return $this->classIn;
    }

    /**
     * Set classQuickly
     *
     * @param string $classQuickly
     *
     * @return TestInglese
     */
    public function setClassQuickly($classQuickly)
    {
        $this->classQuickly = $classQuickly;

        return $this;
    }

    /**
     * Get classQuickly
     *
     * @return string
     */
    public function getClassQuickly()
    {
        return $this->classQuickly;
    }

    /**
     * Set classWonderful
     *
     * @param string $classWonderful
     *
     * @return TestInglese
     */
    public function setClassWonderful($classWonderful)
    {
        $this->classWonderful = $classWonderful;

        return $this;
    }

    /**
     * Get classWonderful
     *
     * @return string
     */
    public function getClassWonderful()
    {
        return $this->classWonderful;
    }

    /**
     * Set classWent
     *
     * @param string $classWent
     *
     * @return TestInglese
     */
    public function setClassWent($classWent)
    {
        $this->classWent = $classWent;

        return $this;
    }

    /**
     * Get classWent
     *
     * @return string
     */
    public function getClassWent()
    {
        return $this->classWent;
    }

    /**
     * Set classOn
     *
     * @param string $classOn
     *
     * @return TestInglese
     */
    public function setClassOn($classOn)
    {
        $this->classOn = $classOn;

        return $this;
    }

    /**
     * Get classOn
     *
     * @return string
     */
    public function getClassOn()
    {
        return $this->classOn;
    }

    /**
     * Set classEnjoy
     *
     * @param string $classEnjoy
     *
     * @return TestInglese
     */
    public function setClassEnjoy($classEnjoy)
    {
        $this->classEnjoy = $classEnjoy;

        return $this;
    }

    /**
     * Get classEnjoy
     *
     * @return string
     */
    public function getClassEnjoy()
    {
        return $this->classEnjoy;
    }

    /**
     * Set classPoor
     *
     * @param string $classPoor
     *
     * @return TestInglese
     */
    public function setClassPoor($classPoor)
    {
        $this->classPoor = $classPoor;

        return $this;
    }

    /**
     * Get classPoor
     *
     * @return string
     */
    public function getClassPoor()
    {
        return $this->classPoor;
    }

    /**
     * Set classFactory
     *
     * @param string $classFactory
     *
     * @return TestInglese
     */
    public function setClassFactory($classFactory)
    {
        $this->classFactory = $classFactory;

        return $this;
    }

    /**
     * Get classFactory
     *
     * @return string
     */
    public function getClassFactory()
    {
        return $this->classFactory;
    }

    /**
     * Set classUsually
     *
     * @param string $classUsually
     *
     * @return TestInglese
     */
    public function setClassUsually($classUsually)
    {
        $this->classUsually = $classUsually;

        return $this;
    }

    /**
     * Get classUsually
     *
     * @return string
     */
    public function getClassUsually()
    {
        return $this->classUsually;
    }

    /**
     * Set classCame
     *
     * @param string $classCame
     *
     * @return TestInglese
     */
    public function setClassCame($classCame)
    {
        $this->classCame = $classCame;

        return $this;
    }

    /**
     * Get classCame
     *
     * @return string
     */
    public function getClassCame()
    {
        return $this->classCame;
    }

    /**
     * Set bestOption1
     *
     * @param string $bestOption1
     *
     * @return TestInglese
     */
    public function setBestOption1($bestOption1)
    {
        $this->bestOption1 = $bestOption1;

        return $this;
    }

    /**
     * Get bestOption1
     *
     * @return string
     */
    public function getBestOption1()
    {
        return $this->bestOption1;
    }

    /**
     * Set bestOption2
     *
     * @param string $bestOption2
     *
     * @return TestInglese
     */
    public function setBestOption2($bestOption2)
    {
        $this->bestOption2 = $bestOption2;

        return $this;
    }

    /**
     * Get bestOption2
     *
     * @return string
     */
    public function getBestOption2()
    {
        return $this->bestOption2;
    }

    /**
     * Set bestOption3
     *
     * @param string $bestOption3
     *
     * @return TestInglese
     */
    public function setBestOption3($bestOption3)
    {
        $this->bestOption3 = $bestOption3;

        return $this;
    }

    /**
     * Get bestOption3
     *
     * @return string
     */
    public function getBestOption3()
    {
        return $this->bestOption3;
    }

    /**
     * Set bestOption4
     *
     * @param string $bestOption4
     *
     * @return TestInglese
     */
    public function setBestOption4($bestOption4)
    {
        $this->bestOption4 = $bestOption4;

        return $this;
    }

    /**
     * Get bestOption4
     *
     * @return string
     */
    public function getBestOption4()
    {
        return $this->bestOption4;
    }

    /**
     * Set bestOption5
     *
     * @param string $bestOption5
     *
     * @return TestInglese
     */
    public function setBestOption5($bestOption5)
    {
        $this->bestOption5 = $bestOption5;

        return $this;
    }

    /**
     * Get bestOption5
     *
     * @return string
     */
    public function getBestOption5()
    {
        return $this->bestOption5;
    }

    /**
     * Set bestOption0
     *
     * @param string $bestOption0
     *
     * @return TestInglese
     */
    public function setBestOption0($bestOption0)
    {
        $this->bestOption0 = $bestOption0;

        return $this;
    }

    /**
     * Get bestOption0
     *
     * @return string
     */
    public function getBestOption0()
    {
        return $this->bestOption0;
    }

    /**
     * Set pair1
     *
     * @param string $pair1
     *
     * @return TestInglese
     */
    public function setPair1($pair1)
    {
        $this->pair1 = $pair1;

        return $this;
    }

    /**
     * Get pair1
     *
     * @return string
     */
    public function getPair1()
    {
        return $this->pair1;
    }

    /**
     * Set pair2
     *
     * @param string $pair2
     *
     * @return TestInglese
     */
    public function setPair2($pair2)
    {
        $this->pair2 = $pair2;

        return $this;
    }

    /**
     * Get pair2
     *
     * @return string
     */
    public function getPair2()
    {
        return $this->pair2;
    }

    /**
     * Set pair3
     *
     * @param string $pair3
     *
     * @return TestInglese
     */
    public function setPair3($pair3)
    {
        $this->pair3 = $pair3;

        return $this;
    }

    /**
     * Get pair3
     *
     * @return string
     */
    public function getPair3()
    {
        return $this->pair3;
    }

    /**
     * Set pair4
     *
     * @param string $pair4
     *
     * @return TestInglese
     */
    public function setPair4($pair4)
    {
        $this->pair4 = $pair4;

        return $this;
    }

    /**
     * Get pair4
     *
     * @return string
     */
    public function getPair4()
    {
        return $this->pair4;
    }

    /**
     * Set pair5
     *
     * @param string $pair5
     *
     * @return TestInglese
     */
    public function setPair5($pair5)
    {
        $this->pair5 = $pair5;

        return $this;
    }

    /**
     * Get pair5
     *
     * @return string
     */
    public function getPair5()
    {
        return $this->pair5;
    }

    /**
     * Set pair0
     *
     * @param string $pair0
     *
     * @return TestInglese
     */
    public function setPair0($pair0)
    {
        $this->pair0 = $pair0;

        return $this;
    }

    /**
     * Get pair0
     *
     * @return string
     */
    public function getPair0()
    {
        return $this->pair0;
    }

    /**
     * Set sentence1
     *
     * @param string $sentence1
     *
     * @return TestInglese
     */
    public function setSentence1($sentence1)
    {
        $this->sentence1 = $sentence1;

        return $this;
    }

    /**
     * Get sentence1
     *
     * @return string
     */
    public function getSentence1()
    {
        return $this->sentence1;
    }

    /**
     * Set sentence2
     *
     * @param string $sentence2
     *
     * @return TestInglese
     */
    public function setSentence2($sentence2)
    {
        $this->sentence2 = $sentence2;

        return $this;
    }

    /**
     * Get sentence2
     *
     * @return string
     */
    public function getSentence2()
    {
        return $this->sentence2;
    }

    /**
     * Set pairText1
     *
     * @param string $pairText1
     *
     * @return TestInglese
     */
    public function setPairText1($pairText1)
    {
        $this->pairText1 = $pairText1;

        return $this;
    }

    /**
     * Get pairText1
     *
     * @return string
     */
    public function getPairText1()
    {
        return $this->pairText1;
    }

    /**
     * Set pairText2
     *
     * @param string $pairText2
     *
     * @return TestInglese
     */
    public function setPairText2($pairText2)
    {
        $this->pairText2 = $pairText2;

        return $this;
    }

    /**
     * Get pairText2
     *
     * @return string
     */
    public function getPairText2()
    {
        return $this->pairText2;
    }

    /**
     * Set pairText3
     *
     * @param string $pairText3
     *
     * @return TestInglese
     */
    public function setPairText3($pairText3)
    {
        $this->pairText3 = $pairText3;

        return $this;
    }

    /**
     * Get pairText3
     *
     * @return string
     */
    public function getPairText3()
    {
        return $this->pairText3;
    }

    /**
     * Set pairText4
     *
     * @param string $pairText4
     *
     * @return TestInglese
     */
    public function setPairText4($pairText4)
    {
        $this->pairText4 = $pairText4;

        return $this;
    }

    /**
     * Get pairText4
     *
     * @return string
     */
    public function getPairText4()
    {
        return $this->pairText4;
    }

    /**
     * Set pairText5
     *
     * @param string $pairText5
     *
     * @return TestInglese
     */
    public function setPairText5($pairText5)
    {
        $this->pairText5 = $pairText5;

        return $this;
    }

    /**
     * Get pairText5
     *
     * @return string
     */
    public function getPairText5()
    {
        return $this->pairText5;
    }

    /**
     * Set pairText0
     *
     * @param string $pairText0
     *
     * @return TestInglese
     */
    public function setPairText0($pairText0)
    {
        $this->pairText0 = $pairText0;

        return $this;
    }

    /**
     * Get pairText0
     *
     * @return string
     */
    public function getPairText0()
    {
        return $this->pairText0;
    }
}
