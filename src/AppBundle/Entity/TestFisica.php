<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Test;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fisica")
 */
class TestFisica extends Test
{
    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $localita;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $luce;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $circuitoLuminoso;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $circuitoEsplode;

    /**
     * Set localita
     *
     * @param string $localita
     *
     * @return TestFisica
     */
    public function setLocalita($localita)
    {
        $this->localita = $localita;

        return $this;
    }

    /**
     * Get localita
     *
     * @return string
     */
    public function getLocalita()
    {
        return $this->localita;
    }

    /**
     * Set luce
     *
     * @param string $luce
     *
     * @return TestFisica
     */
    public function setLuce($luce)
    {
        $this->luce = $luce;

        return $this;
    }

    /**
     * Get luce
     *
     * @return string
     */
    public function getLuce()
    {
        return $this->luce;
    }

    /**
     * Set circuitoLuminoso
     *
     * @param string $circuitoLuminoso
     *
     * @return TestFisica
     */
    public function setCircuitoLuminoso($circuitoLuminoso)
    {
        $this->circuitoLuminoso = $circuitoLuminoso;

        return $this;
    }

    /**
     * Get circuitoLuminoso
     *
     * @return string
     */
    public function getCircuitoLuminoso()
    {
        return $this->circuitoLuminoso;
    }

    /**
     * Set circuitoEsplode
     *
     * @param string $circuitoEsplode
     *
     * @return TestFisica
     */
    public function setCircuitoEsplode($circuitoEsplode)
    {
        $this->circuitoEsplode = $circuitoEsplode;

        return $this;
    }

    /**
     * Get circuitoEsplode
     *
     * @return string
     */
    public function getCircuitoEsplode()
    {
        return $this->circuitoEsplode;
    }
}
