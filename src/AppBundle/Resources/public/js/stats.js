var questions = Object.keys(stats);
questions.forEach(function(question){
    var questionData = stats[question];
    var questionPoints = [];
    Object.keys(questionData).forEach(function(point){
        questionPoints.push(point.substring(1,point.length));
    });
    var myPieChart = new Chart($('#q'+question.split(' ').join('')),{
        type: 'doughnut',
        data: {
            labels: questionPoints,
            datasets: [{
                    data: Object.values(questionData),
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }]
            },
        options: {}
    });
});
