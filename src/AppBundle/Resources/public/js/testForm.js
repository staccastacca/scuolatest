/**
 * Function for ajax auto submit of tests
 *
 * NOTE: path.ajax is required to be set
 */
function autoSaveTestAnswers(){
    $.ajax({
        url: path.testAjax,
        type: "POST",
        data: $('#testform').serializeArray(),
        dataType: "json",
        success: function (result) {},
        error: function (xhr, ajaxOptions, thrownError) {
            Materialize.toast("Errore nel salvataggio automatico delle risposte.", 3000);
        }
    });
}

$('#testform input, #testform select, #testform textarea').change(autoSaveTestAnswers);

/**
 * Timer function for the test
 */
function Countdown(options) {
    var timer,
    instance = this,
    seconds = options.seconds || 0,
    updateStatus = options.onUpdateStatus || function () {},
    counterEnd = options.onCounterEnd || function () {};

    function decrementCounter() {
        if(seconds < 0)
            seconds = 0;
        updateStatus(seconds);
        if (seconds === 0) {
            counterEnd();
            instance.stop();
        }
        seconds--;
    }

    this.start = function () {
        clearInterval(timer);
        timer = 0;
        seconds = options.seconds;
        timer = setInterval(decrementCounter, 1000);
    };

    this.stop = function () {
        clearInterval(timer);
    };
}

var testCounter = new Countdown({
    seconds: time,
    onUpdateStatus: function(sec){
        displayTime(sec);
    },
    onCounterEnd: function(){
        $('#testform').submit();
    }
});
testCounter.start();

function displayTime(sec){
   var twoDigit = function(string){
       if((string+'').length==1) return '0'+string;
       else return string;
   };
   $('#timer').html( twoDigit(Math.floor(sec/3600)) + ':' + twoDigit(Math.floor((sec%3600)/60)) + ':' + twoDigit(sec%60) );
}

$(window).scroll(function() {
    if ($(window).scrollTop() > 180) $('#cooltimer').addClass("fixed");
    else $('#cooltimer').removeClass("fixed");
});
