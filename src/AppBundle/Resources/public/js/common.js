//materialize elements
$(document).ready(function(){
    $(".dropdown-button").dropdown();
    $(".button-collapse").sideNav();
    $('.modal-trigger').modal();
    $('.modal').modal();
    $('select').material_select(); // activate selects
    $('.parallax').parallax();
});

//Disable autocorrect
$('input').on('keypress', function(){
  $(this).val($(this).val());
});

//translations and paths for javascripts
var path = new Object();
var trans = new Object();

function encodeIndianMultiplication(iddi){
    if(iddi == "none") return;
    iddi = '#' + iddi;
    var append = function(str, id){
        var id = iddi+str+id;
        content += ($(id).val() != "" ? $(id).val() : "-") + "#";
    };
    var content = "";
    for(var i=0; i<3; i++){
        for(var j=0; j<4; j++){
            var appendId = function(str){
                append(str,i+'rip'+j);
            };
            appendId('decine');
            appendId('unita');
        }
    }
    for(var i=0; i<7; i++){
        append('risultato', i);
    }
    $(iddi).val(content);
}
