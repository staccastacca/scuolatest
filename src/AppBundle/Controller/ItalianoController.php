<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\TestIta;
use AppBundle\Form\ItalianoType;
use DateInterval;

/**
 * @Route("/italiano")
 */
class ItalianoController extends TestController
{
    const TESTNAME = 'Italiano';
    const TESTDESCRIPTION = 'Svolgi la prova di italiano per verificare le tue competenze di lingua italiana.';
    const TESTIMAGE = 'assets/images/italiano.jpg';
    const ROUTEPREFIX = 'italiano';
    const TESTREPOSITORY = 'AppBundle:TestIta';
    const TESTTEMPLATE = 'tests/italiano.html.twig';
    const MAXMARK = 38;
    const TIMELIMIT = '0:35';
    const RIGHTMARK = 1;
    const WRONGMARK = 0;

    /**
     * Test di italiano
     *
     * @Route("/", name="italiano")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testAction(Request $request)
    {
        return $this->handleTest($request,
            ItalianoType::class, new TestIta());
    }

    /**
     * Ajax route for submitting the result
     *
     * @Route("/ajax", name="italianoAjax")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testAjaxAction(Request $request)
    {
        return $this->handleTest($request,
            ItalianoType::class, new TestIta(), true);
    }

    /**
     * Route di correzione
     *
     * @Route("/fatto", name="italianoSuccess")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testSuccessAction(Request $request) {
        return $this->handleSuccess();
    }

    /**
     * Route for results stats
     *
     * @Route("/risultati", name="italianoResults")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function testResultsAction(Request $request) {
        return $this->handleResults();
    }

    /**
     * Route for CSV results stats
     *
     * @Route("/risultati/csv", name="italianoCsv")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function testCsvAction(Request $request) {
        return $this->handleResultsCSV();
    }

    protected static function calculateMarks($test) {
        return array(
            'Due_punti_A' => self::markAnswer($test->getDuePuntiA(), '3'),
            'Funzione_due_punti_A' => self::markAnswer($test->getDuePuntiFunzioneA(), 'S'),
            'Due_punti_B' => self::markAnswer($test->getDuePuntiB(), '3'),
            'Funzione_due_punti_B' => self::markAnswer($test->getDuePuntiFunzioneB(), 'E'),
            'Due_punti_C' => self::markAnswer($test->getDuePuntiC(), '1'),
            'Funzione_due_punti_C' => self::markAnswer($test->getDuePuntiFunzioneC(), 'S'),
            'Due_punti_D' => self::markAnswer($test->getDuePuntiD(), '2'),
            'Funzione_due_punti_D' => self::markAnswer($test->getDuePuntiFunzioneD(), 'DD'),
            'Due_punti_E' => self::markAnswer($test->getDuePuntiE(), '2'),
            'Funzione_due_punti_E' => self::markAnswer($test->getDuePuntiFunzioneE(), 'ES'),
            'Punteggiatura_A' => self::markAnswerWithRegex($test->getPunteggiaturaA(), '/pop( )+corn( )*:( )*al( )+cinema( )+ce/i'),
            'Punteggiatura_B' => self::markAnswerWithRegex($test->getPunteggiaturaB(), '/il( )+bambino( )+riempiva/i'),
            'Virgola_A' => self::markAnswerWithRegex($test->getVirgolaA(), '/Gli( )+alunni( )*,( )*che( )+hanno( )+ricevuto( )+le( )+credenziali( )+di( )+accesso( )+al( )+registro( )+elettronico( )*,( )*si( )+devono( )+registrare( )+sulla( )+piattaforma( )+d/i'),
            'Virgola_B' => self::markAnswerWithRegex($test->getVirgolaB(), '/Senza( )+le( )+scarpe( )*,( )*che( )+mi( )+ha( )+prestato( )+il( )+mio( )+vicino( )+di( )+casa( )*,( )*non( )+sarei( )+mai( )+riuscito( )+a( )+camminare( )+cosi( )+a( )+lungo/i'),
            'Virgola_C' => self::markAnswerWithRegex($test->getVirgolaC(), '/Gli( )+stranieri( )+adorano( )+visitare( )+le( )+città( )+italiane( )*,( )*che( )+hanno( )+un( )+patrimonio( )+artistico( )+notevole/i'),
            'Lessico_A' => self::markAnswerWithRegex($test->getLessicoA(), '/( )+sufficienza( )*!/i'),
            'Lessico_B' => self::markAnswerWithRegex($test->getLessicoB(), '/( )+tran( )+tran( )*./i'),
            'Lessico_C' => self::markAnswerWithRegex($test->getLessicoC(), '/( )+ferrata( )+/i'),
            'Lessico_D' => self::markAnswerWithRegex($test->getLessicoD(), '/( )+non( )+gli( )+presti( )+/i'),
            'Lessico_E' => self::markAnswerWithRegex($test->getLessicoE(), '/( )+d( )*\'( )*accordo( )+/i'),
            'Lessico_F' => self::markAnswerWithRegex($test->getLessicoF(), '/un( )*\'( )*accelerazione( )+/i'),
            'Lessico_G' => self::markAnswerWithRegex($test->getLessicoG(), '/( )+su( )+/i'),
            'Lessico_H' => self::markAnswerWithRegex($test->getLessicoH(), '/( )+davanti( )+/i'),
            'Lessico_I' => self::markAnswerWithRegex($test->getLessicoI(), '/( )+sugli( )+/i'),
            'Lessico_J' => self::markAnswerWithRegex($test->getLessicoJ(), '/( )+ce( )+n\'è( )+/i'),
            'Plurale_guerra' => self::markAnswerWithRegex($test->getPluraleGuerra(), '/Guerre( )+lampo/i'),
            'Plurale_capoclasse' => self::markAnswerWithRegex($test->getPluraleCapo(), '/Capiclasse/i'),
            'Plurale_cane_poliziotto' => self::markAnswerWithRegex($test->getPluraleCane(), '/Cani( )+poliziotto/i'),
            'Plurale_buono_sconto' => self::markAnswerWithRegex($test->getPluraleBuono(), '/Buoni( )+sconto/i'),
            'Funzione_grammaticale_A' => self::markAnswer($test->getFunzioneGA(), 'B'),
            'Funzione_grammaticale_B' => self::markAnswer($test->getFunzioneGB(), 'B'),
            'Funzione_grammaticale_C' => self::markAnswer($test->getFunzioneGC(), 'B'),
            'Funzione_logica_A' => self::markAnswer($test->getFunzioneLA(), 'A'),
            'Funzione_logica_B' => self::markAnswer($test->getFunzioneLB(), 'B'),
            'Funzione_logica_C' => self::markAnswer($test->getFunzioneLC(), 'B'),
            'Funzione_sintattica_A' => self::markAnswer($test->getFunzioneSA(), 'A'),
            'Funzione_sintattica_B' => self::markAnswer($test->getFunzioneSB(), 'A'),
            'Funzione_sintattica_C' => self::markAnswer($test->getFunzioneSC(), 'A'),
            'Argomento_testo' => self::markAnswer($test->getArgomentoTesto(), 'C', 4),
            'Significato_docilmente' => self::markAnswer($test->getDolcemente(), 'B'),
            'Significato_non_cadde' => self::markAnswer($test->getNonCade(), 'D'),
            'Tipo_sequenza' => self::markAnswer($test->getSequenza(), 'D'),
            'Tempo' => self::markAnswer($test->getTempo(), 'C'),
            'Spazio' => self::markAnswer($test->getSpazio(), 'B'),
            'Narratore' => self::markAnswer($test->getNarratore(), 'B'),
            'Reazione_Buck' => self::markAnswer($test->getBuck(), 'C'),
            'Lessico' => self::markAnswer($test->getLessico(), 'A')
        );
    }
}
