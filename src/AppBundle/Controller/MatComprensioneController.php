<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\TestMatComprensione;
use AppBundle\Form\MatComprensioneType;
use DateTime;
use DateInterval;

/**
 * @Route("/comprensione")
 */
class MatComprensioneController extends TestController
{
    const TESTNAME = 'Comprensione';
    const TESTDESCRIPTION = 'Svogli due quesiti logici non strettamente legati alla scuola.';
    const TESTIMAGE = 'assets/images/comprensione.jpg';
    const ROUTEPREFIX = 'matComprensione';
    const TESTREPOSITORY = 'AppBundle:TestMatComprensione';
    const TESTTEMPLATE = 'tests/matComprensione.html.twig';
    const MAXMARK = 20;
    const TIMELIMIT = '0:35';
    const RIGHTMARK = 5;
    const WRONGMARK = 0;

    /**
     * Test di Matematica: algebra e geometria
     *
     * @Route("/", name="matComprensione")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testAction(Request $request)
    {
        return $this->handleTest($request,
            MatComprensioneType::class, new TestMatComprensione());
    }

    /**
     * Ajax route for submitting the result
     *
     * @Route("/ajax", name="matComprensioneAjax")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testAjaxAction(Request $request)
    {
        return $this->handleTest($request,
            MatComprensioneType::class, new TestMatComprensione(), true);
    }

    /**
     * Route di correzione
     *
     * @Route("/fatto", name="matComprensioneSuccess")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testSuccessAction(Request $request) {
        return $this->handleSuccess();
    }

    /**
     * Route for results stats
     *
     * @Route("/risultati", name="matComprensioneResults")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function testResultsAction(Request $request) {
        return $this->handleResults();
    }

    /**
     * Route for CSV results stats
     *
     * @Route("/risultati/csv", name="matComprensioneCsv")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function testCsvAction(Request $request) {
        return $this->handleResultsCSV();
    }

    protected static function calculateMarks($test) {
        return array(
            'Ora_di_Sydney' => ($test->getOraSydney() >= new DateTime('1970-01-01 16:30') &&
                $test->getOraSydney() <= new DateTime('1970-01-01 18:00') ? static::RIGHTMARK : static::WRONGMARK),
            'Ora_di_Berlino' => ($test->getOraBerlino() >= new DateTime('1970-01-01 7:30') &&
                $test->getOraBerlino() <= new DateTime('1970-01-01 9:00') &&
                $test->getOraBerlino()->diff($test->getOraSydney())->format('%h:%i') == '9:0' ? static::RIGHTMARK : static::WRONGMARK), // quite long but it works
            'Moltiplicazione_indiana' => self::markAnswerWithRegex($test->getMoltiplicazioneIndiana(),
                '/3#5#3#(0|-)#(0|-)#5#4#(0|-)#2#8#2#4#(0|-)#4#3#2#6#3#5#4#(0|-)#9#7#2#4#1#8#2#2#8#2#/', 10, 0)
        );
    }
}
