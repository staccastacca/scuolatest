<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\TestFisica;
use AppBundle\Form\FisicaType;
use DateInterval;

/**
 * @Route("/fisica")
 */
class FisicaController extends TestController
{
    const TESTNAME = 'Fisica';
    const TESTDESCRIPTION = 'Svolgi la prova di fisica per verificare le tue conoscenze in ambito scientifico.';
    const TESTIMAGE = 'assets/images/fisica.jpg';
    const ROUTEPREFIX = 'fisica';
    const TESTREPOSITORY = 'AppBundle:TestFisica';
    const TESTTEMPLATE = 'tests/test.html.twig';
    const MAXMARK = 20;
    const TIMELIMIT = '0:35';
    const RIGHTMARK = 5;
    const WRONGMARK = 0;

    /**
     * Test di Fisica
     *
     * @Route("/", name="fisica")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testAction(Request $request)
    {
        return $this->handleTest($request,
            FisicaType::class, new TestFisica());
    }

    /**
     * Ajax route for submitting the result
     *
     * @Route("/ajax", name="fisicaAjax")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testAjaxAction(Request $request)
    {
        return $this->handleTest($request,
            FisicaType::class, new TestFisica(), true);
    }

    /**
     * Route di correzione
     *
     * @Route("/fatto", name="fisicaSuccess")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testSuccessAction(Request $request) {
        return $this->handleSuccess();
    }

    /**
     * Route for results stats
     *
     * @Route("/risultati", name="fisicaResults")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function testResultsAction(Request $request) {
        return $this->handleResults();
    }

    /**
     * Route for CSV results stats
     *
     * @Route("/risultati/csv", name="fisicaCsv")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function testCsvAction(Request $request) {
        return $this->handleResultsCSV();
    }

    protected static function calculateMarks($test) {
        return array(
            'Treni' => self::markAnswer($test->getLocalita(), 'C'),
            'Velocita_luce' => self::markAnswer($test->getLuce(), 'C'),
            'Circuito' => self::markAnswer($test->getCircuitoLuminoso(), 'A'),
            'Circuito_dannegiato' => self::markAnswer($test->getCircuitoEsplode(), 'C'),
        );
    }
}
