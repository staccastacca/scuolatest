<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\TestType;
use AppBundle\Entity\User;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\StreamedResponse;
use DateTime;
use DateInterval;

/**
 * Abstract class for a test Controller
 */
abstract class TestController extends Controller
{
    /**
     * Returns the name of the route of the test form
     *
     * @return string
     */
    public static function getTestRoute(){
        return static::ROUTEPREFIX;
    }

    /**
     * Returns the name of the route which is used for auto saving the test
     *
     * @return string
     */
    public static function getAjaxRoute(){
        return static::ROUTEPREFIX.'Ajax';
    }

    /**
    * Returns the name of the route of the route in which the mark is shown
    *
    * @return string
    */
    public static function getSuccessRoute(){
        return static::ROUTEPREFIX.'Success';
    }

    /**
     * Returns the name of the doctrine repo for the test entity
     * (E.G. 'AppBundle:TestMatematica')
     *
     * @param EntityManager em
     * @return string
     */
    public static function getTestRepository(EntityManager $em){
        return $em->getRepository(static::TESTREPOSITORY);
    }

    /**
     * Returns the time limit of the test
     *
     * @return integer
     */
    public static function getTimeLimitInSeconds(){
        return strtotime('1970-01-01 '.static::TIMELIMIT) - strtotime('1970-01-01 00:00');
    }

    /**
     * Calculates the marks array given a test entity
     *
     * @param $test
     * @return array
     */
    abstract protected static function calculateMarks($test);

    /**
     * Returns true if the test of the user has been sent
     *
     * @param integer userId
     * @param EntityManager em
     * @return boolean
     */
    private static function checkTest(EntityManager $em, $userId){
        $testRepository = static::getTestRepository($em);

        if(($test = $testRepository->findOneByUserId($userId)) === NULL)
            return false;

        return $test->getSent() ||
            strtotime('now') - $test->getStartTime()->getTimestamp() > static::getTimeLimitInSeconds() + 20;
            // 20 gives some tollerance to the time limit (JUST ON SERVER SIDE. THE CLIENT MUST NOT SEE THIS)
    }

    /**
     * Flag to state the test has not been started yet
     */
    const TEST_NOT_STARTED = 0;

    /**
     * Flag to state the test has been started but it hasn't been sent yet (time not expired yet)
     */
    const TEST_STARTED = 1;

    /**
     * Flag to state the test has been sent or time has expired
     */
    const TEST_SENT = 2;

    /**
     * Returns a integer flag describing the state of the test
     *
     * compare with flags starting with TEST_
     *
     * @param integer userId
     * @param EntityManager em
     * @return string
     */
    public static function getTestStatus(EntityManager $em, $userId){
        $testRepository = static::getTestRepository($em);

        if(($test = $testRepository->findOneByUserId($userId)) === NULL)
            return self::TEST_NOT_STARTED;
        else return self::getTestStatusGivenTest($test);
    }

    /**
     * Marks the user's test otherwise returns null
     *
     * @param integer userId
     * @param EntityManager em
     * @return string
     */
    public static function getTestMarkOrNull(EntityManager $em, $userId){
        if(self::getTestStatus($em,$userId) === self::TEST_SENT){
            $test = static::getTestRepository($em)->findOneByUserId($userId);
            self::calculateMarkIfNecessary($test, $em);
            return $test->getMark();
        }
        else return NULL;
    }

    /**
     * Returns a integer flag describing the state of the test
     *
     * compare with flags starting with TEST_
     *
     * @param test
     * @return string
     */
    public static function getTestStatusGivenTest($test){
        if($test->getSent() || strtotime('now') - $test->getStartTime()->getTimestamp() > static::getTimeLimitInSeconds())
            return self::TEST_SENT;
        else return self::TEST_STARTED;
    }

    /**
     * The test action.
     * NOTE: If the test is not submitted in the time limit, the mark is 0 (but sent remains false).
     *
     * @param Form $form
     * @param Request $request
     */
    protected function handleTest(Request $request, $formClass, $newTestObj, $ajax = false){
        $em = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();

        if($this->checkTest($em, $userId)){
            if($ajax) return new JsonResponse(json_encode(array(
                'result' => 'ko',
                'error' => 'Test cannot be sent',
                'errorCode' => '1',
            )), 403);
            else return $this->redirectToRoute(static::getSuccessRoute());
        }

        $repo = static::getTestRepository($em);


        if(!($testObj = $repo->findOneByUserId($userId))){ //if the test wasn't started before
            // render the form and set start time to current time
            $newTestObj->setSent(false); // not yet sent
            $newTestObj->setUserId($userId);
            $newTestObj->setStartTime(new DateTime('now'));
            $em->persist($newTestObj);
            $em->flush(); //DON'T YOU, FORGET ABOUT MEEEEEEEE
            $testObj = $newTestObj;
        }

        $form = $this->createForm($formClass, $testObj);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            $testObjSent = $form->getData();

            // save the test to the database and set it as sent
            if(!$ajax) $testObjSent->setSent(true);
            $em->flush();

            if($ajax) return new JsonResponse(json_encode(array('result' => 'ok')));
            else return $this->redirectToRoute(static::getSuccessRoute());
        }

        if ($ajax) return new JsonResponse(json_encode(array(
            'result' => 'ko',
            'error' => 'Submit not valid',
            'errorCode' => '2',
        )), 403);
        else return $this->render(static::TESTTEMPLATE, array(
            'form' => $form->createView(),
            'controller' => static::class,
            'timeleft' => $testObj->getStartTime()->getTimestamp()+static::getTimeLimitInSeconds() - strtotime('now')
        ));
    }

    /**
     * Basically the body of the success route for the test
     */
    protected function handleSuccess(){
        $em = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();

        if(!$this->checkTest($em, $userId))
            return $this->redirectToRoute('homepage');

        $testRepository = static::getTestRepository($em);
        $test=$testRepository->findOneByUserId($userId);

        self::calculateMarkIfNecessary($test, $em);

        return $this->render('tests/success.html.twig', array(
            'mark' => $test->getMark(),
            'controller' => static::class,
        ));
    }

    /**
     * Calculates the mark if necessary
     *
     * @param $test
     * @param $em EntityManager
     *
     * @return NULL
     */
    public static function calculateMarkIfNecessary($test, EntityManager $em){
        if(!$test->getMarks() || $test->getMarks() === array() || !$test->getMark()){ // if the mark does to exist calculate it
            $marks = static::calculateMarks($test);
            $test->setMarks($marks);
            $test->setMark(array_sum($marks));
            $test->setSent(true); // the test is definitely sent
            $em->flush(); // DON'T YOU FORGET ABOUT MEEEEEEEE
        }
    }

    /**
     * Stats/results route
     */
    public function handleResults() {
        $stats = $this->calculateStats();

        return $this->render('tests/stats.html.twig', array(
            'test' => static::class,
            'nTests' => $stats['numberOfTestsSubmitted'],
            'markSum' => $stats['sumOfAllMarks'],
            'stats' => $stats['statsPerQuestion'],
        ));
    }

    /**
     * Stats/results csv route
     */
    public function handleResultsCSV(){
        $stats = $this->calculateStats();

        $response = new StreamedResponse(function() use($stats) {
            $handle = fopen('php://output', 'r+');

            $statsQ = $stats['statsPerQuestion'];

            fputcsv($handle, array('Statistiche di '.static::TESTNAME));
            fputcsv($handle, array('File generato con Scuolatest, piattaforma '.
                'creata da Claudio Maggioni, Giorgio Croci e Davide Fiori.'));
            fputcsv($handle, array(''));
            fputcsv($handle, array(
                'Numero candidati: ',
                $stats['numberOfTestsSubmitted'],
            ));
            fputcsv($handle, array(
                'Media punteggio: ',
                ($stats['numberOfTestsSubmitted'] === 0 ? 'Non calcolabile' :
                    $stats['sumOfAllMarks']/$stats['numberOfTestsSubmitted']),
            ));
            fputcsv($handle, array(
                'Sufficienza: ',
                static::PASSMARK,
            ));
            fputcsv($handle, array(''));
            fputcsv($handle, array(
                'Domanda',
                'Punteggio ottenuto',
                'Numero candidati',
            ));
            foreach($statsQ as $question => $statQ){
                fputcsv($handle, array($question));
                foreach($statQ as $points => $people)
                fputcsv($handle, array(
                    NULL,
                    substr($points, 1),
                    $people
                ));
            }

            fclose($handle);
        });

        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition','attachment; filename="'.static::TESTNAME.'.csv"');
        return $response;
    }

    /**
     * Calculates the tests stats
     *
     * @return array
     */
    private function calculateStats(){
        $em = $this->getDoctrine()->getManager();
        $testRepository = static::getTestRepository($em);

        $tests = $testRepository->findAll();

        $markSum = 0;
        $results = array();
        foreach($tests as $test)
            if(static::getTestStatusGivenTest($test) === static::TEST_SENT){
                static::calculateMarkIfNecessary($test, $em);
                $results[] = $test->getMarks();
                $markSum += $test->getMark();
            }

        $stats = array();

        if($results)
            foreach(array_keys($results[0]) as $i){
                $stat = array();
                foreach($results as $result){
                    if(array_key_exists('p'.$result[$i], $stat)) $stat['p'.$result[$i]]++;
                    else $stat['p'.$result[$i]]=1;
                }
                $stats[str_replace('_', ' ',$i)] = $stat;
            }

        return array(
            'numberOfTestsSubmitted' => count($results),
            'sumOfAllMarks' => $markSum,
            'statsPerQuestion' => $stats,
        );
    }

    /**
     * Returns the mark got in an answer given the answer given and the correct
     * answer (and optionally particular right mark and wrong mark)
     *
     * @param $answer
     * @param $rightAnswer
     * @param integer $rightMark = NULL
     * @param integer $wrongMark = NULL
     * @return integer
     */
    protected static function markAnswer($answer, $rightAnswer, $rightMark = NULL, $wrongMark = NULL){
        if($rightMark === NULL) $rightMark=static::RIGHTMARK;
        if($wrongMark === NULL) $wrongMark=static::WRONGMARK;

        return $answer === $rightAnswer ? $rightMark : $wrongMark; // to little sleep here
        //really want to use Scala here...
    }

    /**
     * Returns the mark got in an answer given the answer given and the correct
     * answer (and optionally particular right mark and wrong mark)
     * Uses regex for comparison ($rightAnsqwer is an expression)
     *
     * @param $answer
     * @param $rightAnswer
     * @param integer $rightMark = NULL
     * @param integer $wrongMark = NULL
     * @return integer
     */
    protected static function markAnswerWithRegex($answer, $rightAnswer, $rightMark = NULL, $wrongMark = NULL){
        if($rightMark === NULL) $rightMark=static::RIGHTMARK;
        if($wrongMark === NULL) $wrongMark=static::WRONGMARK;

        return preg_match($rightAnswer, $answer) ? $rightMark : $wrongMark; // here too
    }
}
