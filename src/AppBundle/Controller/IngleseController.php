<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\TestInglese;
use AppBundle\Form\IngleseType;

/**
 * @Route("/inglese")
 */
class IngleseController extends TestController
{
    const TESTNAME = 'Inglese';
    const TESTDESCRIPTION = 'Verifica la tua conoscenza della lingua inglese.';
    const TESTIMAGE = 'assets/images/inglese.jpg';
    const ROUTEPREFIX = 'inglese';
    const TESTREPOSITORY = 'AppBundle:TestInglese';
    const TESTTEMPLATE = 'tests/inglese.html.twig';
    const MAXMARK = 35;
    const TIMELIMIT = '0:35';
    const RIGHTMARK = 1;
    const WRONGMARK = 0;

    /**
     * Test di Fisica
     *
     * @Route("/", name="inglese")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testAction(Request $request)
    {
        return $this->handleTest($request,
            IngleseType::class, new TestInglese());
    }

    /**
     * Ajax route for submitting the result
     *
     * @Route("/ajax", name="ingleseAjax")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testAjaxAction(Request $request)
    {
        return $this->handleTest($request,
            IngleseType::class, new TestInglese(), true);
    }

    /**
     * Route di correzione
     *
     * @Route("/fatto", name="ingleseSuccess")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testSuccessAction(Request $request) {
        return $this->handleSuccess();
    }

    /**
     * Route for results stats
     *
     * @Route("/risultati", name="ingleseResults")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function testResultsAction(Request $request) {
        return $this->handleResults();
    }

    /**
     * Route for CSV results stats
     *
     * @Route("/risultati/csv", name="ingleseCsv")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function testCsvAction(Request $request) {
        return $this->handleResultsCSV();
    }

    const classAns = array('N', 'P', 'D', 'A', 'V', 'P', 'V', 'A', 'N', 'D', 'V');
    const bestAns = array('B', 'B', 'B', 'A', 'B', 'B');
    const pairAns = array('E', 'D', 'A', 'F', 'B', 'C');
    const pairTextAns = array('/What/i', '/Who/i', '/Whose/i', '/Where/i', '/When/i', '/Which/i');

    protected static function calculateMarks($test) {
        $marks = array();

        foreach(IngleseType::wordsToClassify as $i => $wordToClassify)
            $marks['Classe_'.$wordToClassify] =
                self::markAnswer($test->{'getClass'.$wordToClassify}(), self::classAns[$i]);

        foreach(IngleseType::bestOptions as $i => $bestOption)
            $marks['Opzione_migliore_'.($i+1)] =
                self::markAnswer($test->{'getBestOption'.$i}(), self::bestAns[$i]);

        foreach(IngleseType::matchSentences as $i => $matchSentence){
            $marks['Abbinamento_inserimento_'.($i+1)] =
                self::markAnswerWithRegex($test->{'getPairText'.$i}(), self::pairTextAns[$i]);
            $marks['Abbinamento_'.($i+1)] =
                self::markAnswer($test->{'getPair'.$i}(), self::pairAns[$i]);
        }

        $marks['Frase_1'] = self::markAnswerWithRegex($test->getSentence1(),
            '/I( )+think( )+that( )+you( )+are( )+a( )+good( )+student( )*.( )*Am( )+I( )+right( )*?/i', 3);
        $marks['Frase_2'] = self::markAnswerWithRegex($test->getSentence2(),
            '/My( )+house( )+is( )+on( )+the( )+top( )+of( )+a( )+hill( )+near( )+a( )+lake( )*./i', 3);

        return $marks;
    }
}
