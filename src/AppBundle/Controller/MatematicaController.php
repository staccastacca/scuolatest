<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\TestMatematica;
use AppBundle\Form\MatematicaType;
use DateTime;
use DateInterval;

/**
 * @Route("/matematica")
 */
class MatematicaController extends TestController
{
    const TESTNAME = 'Matematica';
    const TESTDESCRIPTION = 'Svolgi la prova di algebra e geometria per verificare la tua capacità di ragionamento.';
    const TESTIMAGE = 'assets/images/matematica.jpg';
    const ROUTEPREFIX = 'matematica';
    const TESTREPOSITORY = 'AppBundle:TestMatematica';
    const TESTTEMPLATE = 'tests/matematica.html.twig';
    const MAXMARK = 106;
    const TIMELIMIT = '1:10';
    const RIGHTMARK = 5;
    const WRONGMARK = 0;

    /**
     * Test di Matematica: algebra e geometria
     *
     * @Route("/", name="matematica")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testAction(Request $request)
    {
        return $this->handleTest($request,
            MatematicaType::class, new TestMatematica());
    }

    /**
     * Ajax route for submitting the result
     *
     * @Route("/ajax", name="matematicaAjax")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testAjaxAction(Request $request)
    {
        return $this->handleTest($request,
            MatematicaType::class, new TestMatematica(), true);
    }

    /**
     * Route di correzione
     *
     * @Route("/fatto", name="matematicaSuccess")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function testSuccessAction(Request $request) {
        return $this->handleSuccess();
    }

    /**
     * Route for results stats
     *
     * @Route("/risultati", name="matematicaResults")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function testResultsAction(Request $request) {
        return $this->handleResults();
    }

    /**
     * Route for CSV results
     *
     * @Route("/risultati/csv", name="matematicaCsv")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function testCsvAction(Request $request) {
        return $this->handleResultsCSV();
    }

    protected static function calculateMarks($test) {
        return array(
            'Angoli' => self::markAnswer($test->getAngoli(), 'B'),
            'Rombo' => self::markAnswer($test->getRombo(), 'C'),
            'Cilindri_di_carta' => self::markAnswer($test->getFoglioCarta(), 'C'),
            'Semicerchio' => self::markAnswer($test->getSemicerchio(), 'A'),
            'Vero_falso_geometria_1' => self::markAnswer($test->getVeroFalsoGeometria1(), true, 2),
            'Vero_falso_geometria_2' => self::markAnswer($test->getVeroFalsoGeometria2(), true, 2),
            'Vero_falso_geometria_3' => self::markAnswer($test->getVeroFalsoGeometria3(), true, 2),
            'Vero_falso_geometria_4' => self::markAnswer($test->getVeroFalsoGeometria4(), false, 2),
            'Proporizionalita_diretta' => self::markAnswer($test->getRelazioni(), 'A'),
            'Perimetro_triangolo' => self::markAnswer($test->getDistanzaPunti(), 'D'),
            'Triangoli' => self::markAnswer($test->getTriangoli(), 'C'),
            'Centro_di_simmetria' => self::markAnswer($test->getSimmetria(), 'B'),
            'Grafico_a_torta' => self::markAnswer($test->getCirconferenza(), 'C'),
            'Frazione_fattori_primi' => self::markAnswer($test->getFattoriPrimi(), 'B'),
            'Vero_falso_MCD_e_mcm_1' => self::markAnswer($test->getMCDormcm1(), false, 2),
            'Vero_falso_MCD_e_mcm_2' => self::markAnswer($test->getMCDormcm2(), false, 2),
            'Vero_falso_MCD_e_mcm_3' => self::markAnswer($test->getMCDormcm3(), true, 2),
            'Vero_falso_MCD_e_mcm_4' => self::markAnswer($test->getMCDormcm4(), false, 2),
            'Insiemi_N_Z_Q_1' => self::markAnswer($test->getTabNum1(), 'C', 1),
            'Insiemi_N_Z_Q_2' => self::markAnswer($test->getTabNum2(), 'B', 1),
            'Insiemi_N_Z_Q_3' => self::markAnswer($test->getTabNum3(), 'C', 1),
            'Insiemi_N_Z_Q_4' => self::markAnswer($test->getTabNum4(), 'B', 1),
            'Insiemi_N_Z_Q_5' => self::markAnswer($test->getTabNum5(), 'C', 1),
            'Insiemi_N_Z_Q_6' => self::markAnswer($test->getTabNum6(), 'A', 1),
            'Primi' => self::markAnswer($test->getSiNoPrimi(), false, 2),
            'Primi_tra_loro' => self::markAnswer($test->getSiNoPrimiLoro(), true, 2),
            'Un_mezzo_alla_50' => self::markAnswer($test->getUnMezzo(), 'D'),
            'Affermazione_razionale' => self::markAnswer($test->getNumeroRazionale(), 'D'),
            'Farmaco_a_gocce' => self::markAnswer($test->getMedicina(), 'C'),
            'Tagliatelle' => self::markAnswer($test->getCannavacciuolo(), 'D'),
            'Soluzione_equazione' => self::markAnswer($test->getEquazione(), 'B'),
            'Espressione_in_prosa' => self::markAnswer($test->getSuccessivo(), 'A')
        );
    }
}
