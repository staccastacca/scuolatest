<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Form\FeedbackType;
use AppBundle\Entity\Feedback;

class DefaultController extends Controller
{
    /**
     * Returns all the tests registered
     */
    public static function getTestClasses(){
        return array(
            MatematicaController::class,
            ItalianoController::class,
            FisicaController::class,
            MatComprensioneController::class,
            IngleseController::class,
        );
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            return $this->render('index_public.html.twig');

        $em = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();

        $testResults = array();
        $allDone = true;
        $testClasses = self::getTestClasses();
        $totalmark = 0;
        $totalMaxMark = 0;
        foreach($testClasses as $testClass){
            $testStatus = $testClass::getTestStatus($em, $userId);
            if($testStatus !== TestController::TEST_SENT) $allDone = false;
            $mark = $testClass::getTestMarkOrNull($em, $userId);
            $testResults[] = array(
                'status' => $testStatus,
                'class' => $testClass,
                'mark' => $mark
            );
            $totalmark += $mark;
            $totalMaxMark += $testClass::MAXMARK;
        }

        $totalmark = ($totalmark / $totalMaxMark)*100;

        $form = NULL;
        if($allDone){
            $feedback = $em->getRepository('AppBundle:Feedback')
                ->findOneByUserId($userId);
            if(($isNewFeedback = !$feedback)){
                $feedback = new Feedback();
                $feedback->setUserId($userId);
            }

            $form = $this->createForm(FeedbackType::class, $feedback);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $feedbackSubmitted = $form->getData();

                $feedback->setFeed($feedbackSubmitted->getFeed());
                if($isNewFeedback) $em->persist($feedback);
                $em->flush();

                $request->getSession()->getFlashBag()
                    ->add('success', 'Feedback inviato con successo!');
            }
        }

        return $this->render('index.html.twig', array(
            'tests' => $testResults,
            'allDone' => $allDone,
            'totalMark' => $totalmark,
            'feedbackForm' => $form ? $form->createView() : NULL,
        ));
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Route("/risultati", name="results")
     */
    public function resultsAction(Request $request)
    {
        return $this->render('results.html.twig', array(
            'tests' => self::getTestClasses(),
        ));
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Route("/risultato-finale", name="finalresult")
     */
    public function finalResultAction(Request $request)
    {
        $finalMark = 0; $maxmarkSum = 0;
        $allDone = true;
        $testClasses = self::getTestClasses();

        $em = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();

        foreach($testClasses as $testClass){
            if($testClass::getTestStatus($em, $userId) !== TestController::TEST_SENT) {
              $allDone = false;
              break;
            }
            $maxmarkSum += $testClass::MAXMARK;
            $finalMark += $testClass::getTestMarkOrNull($em, $userId);
        }

        if($allDone){
          if($finalMark >= (0.8 * $maxmarkSum)) $band = 2;
          if($finalMark >= (0.5 * $maxmarkSum)) $band = 1;
          else $band = 0;
        }
        else $band = -1;

        return $this->render('finalresult.html.twig', array(
            'band' => $band,
        ));
    }

    /**
     * @Route("/noscript", name="noscript")
     */
    public function noscriptAction(Request $request)
    {
        return $this->render('public/noscript.html.twig');
    }

    /**
     * @Route("/about", name="about")
     */
    public function aboutAction(Request $request)
    {
        return $this->render('about.html.twig');
    }
}
