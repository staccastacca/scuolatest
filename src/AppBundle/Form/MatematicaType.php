<?php

namespace AppBundle\Form;

use AppBundle\Form\Type\ImageChoiceType;
use AppBundle\Form\Type\TextChoiceType;
use AppBundle\Form\Type\TrueFalseType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MatematicaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('angoli', ImageChoiceType::class, array(
                'choices'  => array(
                    'α=180-(γ-β)' => 'A',
                    'α=γ-β' => 'B',
                    'α=180-(γ+β)' => 'C',
                    'α=γ+β' => 'D',
                ),
                'base_path' => 'assets/images/angoli.png',
                'question' => 'Quale delle seguenti relazioni è vera?',
            ))
            ->add('rombo', TextChoiceType::class, array(
                'choices'  => array(
                    'Gli angoli opposti sono congruenti' => 'A',
                    'I lati sono tutti congruenti' => 'B',
                    'La diagonale minore lo divide in due triangoli equilateri' => 'C',
                    'Le diagonali si intersecano nel loro punto medio' => 'D',
                ),
                'question' => 'Quale tra le seguenti affermazioni relative ad un rombo è falsa?',
            ))
            ->add('foglioCarta', TextChoiceType::class, array(
                'choices'  => array(
                    'I due cilindri hanno la stessa superficie di base' => 'A',
                    'I due cilindri hanno lo stesso volume' => 'B',
                    'I due cilindri hanno la stessa superficie laterale' => 'C',
                    'I due cilindri hanno la stessa superficie totale' => 'D',
                ),
                'question' => 'Con un foglio di carta rettangolare possiamo costruire due cilindri diversi. Quale tra le seguenti affermazioni è vera?',
            ))
            ->add('semicerchio', ImageChoiceType::class, array(
                'choices'  => array(
                    '10A' => 'A',
                    '8A' => 'B',
                    '12A' => 'C',
                    '14A' => 'D',
                ),
                'base_path' => 'assets/images/semicerchio.png',
                'question' => 'Quanto misura l\'area della parte arancione rispetto all\'area A del semicerchio rosa a sinistra?',
            ))
            ->add('veroFalsoGeometria1', TrueFalseType::class, array(
                'question' => 'Il quadrilatero ABED è equivalente al triangolo ACD'
            ))
            ->add('veroFalsoGeometria2', TrueFalseType::class, array(
                'question' => 'Il triangolo BCD è equivalente al triangolo BED'
            ))
            ->add('veroFalsoGeometria3', TrueFalseType::class, array(
                'question' => 'Il triangolo BCD e il triangolo BED hanno un\'altezza congruente, quella rispetto al lato BD'
            ))
            ->add('veroFalsoGeometria4', TrueFalseType::class, array(
                'question' => 'Il triangolo BCF e il triangolo FDE non sono equivalenti'
            ))

            ->add('relazioni', ImageChoiceType::class, array(
                'choices'  => array(
                    'y=3x' => 'A',
                    'y=(1/3)x' => 'B',
                    'y=-(1/3)x' => 'C',
                    'y=-3x' => 'D',
                ),
                'base_path' => 'assets/images/relazioni.png',
                'question' => 'Quale fra le seguenti relazioni che esprimono una proporzionalità diretta è rappresentata nel grafico in figura?',
            ))
            ->add('distanzaPunti', ImageChoiceType::class, array(
                'choices'  => array(
                    '166' => 'A',
                    '207' => 'B',
                    '190' => 'C',
                    '180' => 'D',
                ),
                'base_path' => 'assets/images/distanzapunti.png',
                'question' => 'La distanza tra il punto B e la retta <em>f</em> '.
                    'misura 36cm; le distanze AB e BC misurano 45 e 85 cm. '.
                    'Il perimetro del triangolo ABC è, in cm:',
            ))
            ->add('triangoli', ImageChoiceType::class, array(
                'choices'  => array(
                    '112 cm' => 'A',
                    '96 cm' => 'B',
                    '252 cm' => 'C',
                    '192 cm' => 'D',
                ),
                'base_path' => 'assets/images/triangoli.png',
                'question' => 'I triangoli in figura sono simili e il rapporto tra due lati corrispondenti è 2/3. Se il perimetro di A\'B\'C\' è 168 cm, qual è il perimetro di ABC?',
            ))
            ->add('simmetria', ImageChoiceType::class, array(
                'choices'  => array(
                    '(0;0)' => 'A',
                    '(2;2)' => 'B',
                    '(-1;1)' => 'C',
                    '(1;-1)' => 'D',
                ),
                'base_path' => 'assets/images/simmetria.png',
                'question' => 'Il centro di simmetria che trasforma uno dei due quadrilateri nell\'altro è:',
            ))
            ->add('circonferenza', ImageChoiceType::class, array(
                'choices'  => array(
                    '10 ragazzi preferiscono il tennis' => 'A',
                    'Il 70% dei ragazzi preferisce il calcio o il tennis' => 'B',
                    'Il 60% dei ragazzi non preferisceil calcio' => 'C',
                    'Il 40% dei ragazzi preferisce un altro sport' => 'D',
                ),
                'base_path' => 'assets/images/circonferenza.png',
                'question' => 'Da un\'intervista eseguita su un gruppo di 200 studenti è risultato quanto riportato nell\'aereogramma. Possiamo dire che:',
            ))
            ->add('fattoriPrimi', TextChoiceType::class, array(
                'choices'  => array(
                    '1/30' => 'A',
                    '4/15' => 'B',
                    '2/15' => 'C',
                    '4/45' => 'D',
                ),
                'question' => 'Scomponi in fattori primi e semplifica: (64*243*120)/(81*128*27*25). Il risultato è:',
            ))
            ->add('MCDormcm1', TrueFalseType::class, array(
                'question' => 'Il M.C.D. fra 2 numeri primi è 0'
            ))
            ->add('MCDormcm2', TrueFalseType::class, array(
                'question' => 'Il M.C.D. fra 2 numeri è divisibile per entrambi i numeri'
            ))
            ->add('MCDormcm3', TrueFalseType::class, array(
                'question' => 'Il m.c.m fra 2 o più numeri primi è maggiore di ciascuno di essi'
            ))
            ->add('MCDormcm4', TrueFalseType::class, array(
                'question' => 'Se il m.c.m. tra a e b è uguale ad a*b, allora a*b è numero primo'
            ))
            ->add('tabNum1', TextChoiceType::class, array(
                'choices'  => array(
                    'N' => 'A',
                    'Z' => 'B',
                    'Q' => 'C',
                ),
                'question' => '7/4',
                'embed' => true,
            ))
            ->add('tabNum2', TextChoiceType::class, array(
                'choices'  => array(
                    'N' => 'A',
                    'Z' => 'B',
                    'Q' => 'C',
                ),
                'question' => '-(12/3)',
                'embed' => true,
            ))
            ->add('tabNum3', TextChoiceType::class, array(
                'choices'  => array(
                    'N' => 'A',
                    'Z' => 'B',
                    'Q' => 'C',
                ),
                'question' => '1.155555...',
                'embed' => true,
            ))
            ->add('tabNum4', TextChoiceType::class, array(
                'choices'  => array(
                    'N' => 'A',
                    'Z' => 'B',
                    'Q' => 'C',
                ),
                'question' => '-√(25)',
                'embed' => true,
            ))
            ->add('tabNum5', TextChoiceType::class, array(
                'choices'  => array(
                    'N' => 'A',
                    'Z' => 'B',
                    'Q' => 'C',
                ),
                'question' => '25%',
                'embed' => true,
            ))
            ->add('tabNum6', TextChoiceType::class, array(
                'choices'  => array(
                    'N' => 'A',
                    'Z' => 'B',
                    'Q' => 'C',
                ),
                'question' => '0/8',
                'embed' => true,
            ))

            ->add('siNoPrimi', TrueFalseType::class, array(
                'question' => 'I due numeri sono primi'
            ))
            ->add('siNoPrimiLoro', TrueFalseType::class, array(
                'question' => 'I due numeri sono primi fra loro'
            ))
            ->add('unMezzo', TextChoiceType::class, array(
                'choices'  => array(
                    '(1/2)^49' => 'A',
                    '(1/4)^50' => 'B',
                    '(1/2)^25' => 'C',
                    '(1/2)^51' => 'D',
                ),
                'question' => 'La metà di (1/2)^50 è:',
            ))
            ->add('numeroRazionale', TextChoiceType::class, array(
                'choices'  => array(
                    'L’affermazione è vera perché la moltiplicazione è un’operazione che restituisce sempre un valore più grande di quello di partenza' => 'A',
                    'L’affermazione è vera perché se eleviamo alla seconda un qualsiasi numero razionale otteniamo sempre un numero più grande di quello iniziale' => 'B',
                    'E’ falsa' => 'C',
                    'E’ impossibile stabilire se l’affermazione è vera o falsa' => 'D',
                ),
                'question' => 'Considera l’affermazione “se si moltiplica un numero razionale qualsiasi per se stesso si ottiene sempre un numero più grande di quello iniziale.“',
            ))
            ->add('medicina', TextChoiceType::class, array(
                'choices'  => array(
                    '200 giorni' => 'A',
                    '150 giorni' => 'B',
                    '100 giorni' => 'C',
                    '10 giorni' => 'D',
                ),
                'question' => 'Una medicina viene presa per due volte al giorno in 25 gocce. Se il flacone contiene 2500 mm<sup>3</sup> di farmaco e ogni goccia ha un volume di 0,5 mm<sup>3</sup>, dopo quanti giorni il flacone sarà esaurito?',
            ))
            ->add('cannavacciuolo', TextChoiceType::class, array(
                'choices'  => array(
                    '10 hg' => 'A',
                    '15 hg' => 'B',
                    '2 Kg' => 'C',
                    '2.125 Kg' => 'D',
                ),
                'question' => 'Un cuoco prepara un piatto di tagliatelle per un pranzo di 34 persone. La ricetta prevede 2,50 hg di tagliatelle per 4 persone. Quante tagliatelle deve usare il cuoco per 34 persone?',
            ))
            ->add('equazione', TextChoiceType::class, array(
                'choices'  => array(
                    'x=1/2' => 'A',
                    'x=0' => 'B',
                    'x=-(1/2)' => 'C',
                    'x=-2' => 'D',
                ),
                'question' => 'Quale delle seguenti è la soluzione dell’equazione 2x=0?',
            ))
            ->add('successivo', TextChoiceType::class, array(
                'choices'  => array(
                    '6' => 'A',
                    '11' => 'B',
                    '30' => 'C',
                    '2' => 'D',
                ),
                'question' => 'Al successivo di a aggiungi il precedente di b moltiplicato per la differenza tra a e b. Se a=5 e b=1, il risultato è:',
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'consegna'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TestMatematica',
            'required' => false
        ));
    }
}
