<?php

namespace AppBundle\Form;

use AppBundle\Form\Type\EditTextInputType;
use AppBundle\Form\Type\TextChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IngleseType extends AbstractType
{
    const wordsToClassify = array(
        'Computer', 'In', 'Quickly', 'Wonderful', 'Went', 'On', 'Enjoy',
        'Poor', 'Factory', 'Usually', 'Came'
    );

    const bestOptions = array(
        array(
            'question' => 'Sorry I can\'t come tomorrow.',
            'A' => 'Not important.',
            'B' => 'Never mind.'
        ),
        array(
            'question' => 'Sorry I\'m late.',
            'A' => 'Don\'t mention it.',
            'B' => 'It doesn\'t matter.'
        ),
        array(
            'question' => 'How do you do?',
            'A' => 'How do you do?',
            'B' => 'Fine, thank you.'
        ),
        array(
            'question' => 'Thank you very much.',
            'A' => 'Not at all.',
            'B' => 'Never mind.'
        ),
        array(
            'question' => 'Can I help you?',
            'A' => 'Of course I can.',
            'B' => 'No, thank you. I\'m just looking.'
        ),
        array(
            'question' => 'Cheers!',
            'A' => 'And you.',
            'B' => 'Cheers!'
        )
    );

    const matchSentences = array(
        '[HERE]\'s this in english?', '[HERE] is your teacher?',
        '[HERE] book is it?', '[HERE] did you park?',
        '[HERE] did you go on holiday?', '[HERE] bag is yours?'
    );

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach(self::wordsToClassify as $wordToClassify)
            $builder->add('class'.$wordToClassify, TextChoiceType::class, array(
                'question' => '<em>'.$wordToClassify.'</em>',
                'choices' => array(
                    'Noun' => 'N',
                    'Adjective' => 'A',
                    'Verb' => 'V',
                    'Adverb' => 'D',
                    'Preposition' => 'P'
                ),
                'embed' => true,
            ));

        foreach(self::bestOptions as $i => $bestOption)
            $builder->add('bestOption'.$i, TextChoiceType::class, array(
                'question' => '<em>'.$bestOption['question'].'</em>',
                'choices' => array(
                    $bestOption['A'] => 'A',
                    $bestOption['B'] => 'B'
                ),
                'embed' => true,
            ));

        foreach(self::matchSentences as $i => $matchSentence)
            $builder
                ->add('pairText'.$i, EditTextInputType::class, array(
                    'hint' => $matchSentence,
                    'embed' => true,
                ))
                ->add('pair'.$i, TextChoiceType::class, array(
                    'question' => 'Pair the sentence above with the best option.',
                    'choices' => array(
                        'It\'s his.' => 'A',
                        'A month ago.' => 'B',
                        'The small blue one.' => 'C',
                        'Marie Evans.' => 'D',
                        'It\'s a bicycle.' => 'E',
                        'Near the station.' => 'F'
                    ),
                    'expanded' => false,
                    'embed' => true,
                ));

        $builder
            ->add('sentence1', EditTextInputType::class, array(
                'hint' => 'I right / that / Am / a good / you / student. / I think / are / ?'
            ))
            ->add('sentence2', EditTextInputType::class, array(
                'hint' => 'lake. / on / My / is / hill / a / house / a / top / near / the'
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'consegna'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TestInglese',
            'required' => false
        ));
    }
}
