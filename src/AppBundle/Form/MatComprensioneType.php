<?php

namespace AppBundle\Form;

use AppBundle\Form\Type\TextChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use DateTime;

class MatComprensioneType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sydneyBerlino', TextChoiceType::class, array(
                'choices'  => array(
                    '04:00' => 'A',
                    '22:00' => 'B',
                    '10:00' => 'C',
                    '09:00' => 'D',
                ),
                'question' => 'Quando sono le 19:00 a Sydney che ora è a Berlino?',
                'embed' => true,
            ))
            ->add('oraSydney', TimeType::class, array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'data' => new Datetime('00:00'),
                'label' => 'Ora di Sydney',
            ))
            ->add('oraBerlino', TimeType::class, array(
                'input'  => 'datetime',
                'widget' => 'single_text',
                'data' => new Datetime('00:00'),
                'label' => 'Ora di Berlino'
            ))
            ->add('moltiplicazioneIndiana', HiddenType::class)
            ->add('submit', SubmitType::class, array(
                'label' => 'consegna'
            ))
        ;
 }

 /**
  * @param OptionsResolver $resolver
  */
 public function configureOptions(OptionsResolver $resolver)
 {
    $resolver->setDefaults(array(
        'data_class' => 'AppBundle\Entity\TestMatComprensione',
        'required' => false
    ));
 }
}
