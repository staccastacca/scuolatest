<?php

namespace AppBundle\Form;

use AppBundle\Form\Type\EditTextType;
use AppBundle\Form\Type\EditTextInputType;
use AppBundle\Form\Type\TextChoiceType;
use AppBundle\Form\Type\InfixChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItalianoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $duePuntiFrasi = array(
            'A' => 'Stamattina # non ho fatto # colazione # mi sono alzato troppo tardi.',
            'B' => 'In cartella # ho messo tutto il materiale che mi serve # per domani # riga, compasso, squadra e righello. ',
            'C' => 'Lungo l\'itinerario manzoniano si possono trovare delle targhe # vi sono scritti # brani del romanzo “I promessi sposi” e spiegazioni di tipo # artistico e architettonico dei monumenti.',
            'D' => 'L\'insegnante di matematica, ogni volta che entra in classe, pone agli alunni sempre # la stessa dornanda # "Tutti avete fatto # i compiti per oggi?" ',
            'E' => 'Nel megastore aperto recentemente in piazza Castello si può trovare # di tutto # vestiti, elettrodomestici, articoli # per il giardinaggio... ',
        );

        for($i = 'A'; $i <= 'E'; $i++){
            $builder->add('duePunti'.$i, InfixChoiceType::class, array(
                'sentence' => $duePuntiFrasi[$i],
                'choices' => InfixChoiceType::generatePositionChoiceArray(3),
            ))
            ->add('duePuntiFunzione'.$i, TextChoiceType::class, array(
                'question' => 'Nella frase sovrastante, qual è la funzione dei due punti?',
                'choices' => array(
                    'Elenco' => 'E',
                    'Discorso diretto' => 'DD',
                    'Spiegazione' => 'S',
                    'Esempio' => 'ES'
                ),
                'embed' => true,
            ));
        }

        $builder
            ->add('punteggiaturaA', EditTextType::class, array(
                'toEdit' => 'Ho voglia di mangiare pop corn: al cinema: ce ne sono a disposizione degli spettatori sempre in quantità.'
            ))
            ->add('punteggiaturaB', EditTextType::class, array(
                'toEdit' => 'Mentre la mamma sgridava, il bambino: riempiva il bicchiere d\'acqua.'
            ))
            ->add('virgolaA', EditTextType::class, array(
                'toEdit' => 'Gli alunni che hanno ricevuto le credenziali di accesso al registro elettronico si devono registrare sulla piattaforma d\'istituto.',
                'hint' => '(Solo alcuni alunni hanno ricevuto le credenziali)'
            ))
            ->add('virgolaB', EditTextType::class, array(
                'toEdit' => 'Senza le scarpe che mi ha prestato il mio vicino di casa non sarei mai riuscito a camminare cosi a lungo.',
                'hint' => '(intendi dire che a piedi nudi non ce l\'avresti fatta)'
            ))
            ->add('virgolaC', EditTextType::class, array(
                'toEdit' => 'Gli stranieri adorano visitare le città italiane che hanno un patrimonio artistico notevole.',
                'hint' => '(tutte ie citta d\'Italia sono artisticamente degne di nota)'
            ))
            ->add('lessicoA', EditTextType::class, array(
                'toEdit' => 'Nell\'interrogazione di storia avrò preso almeno la sufficenza!'
            ))
            ->add('lessicoB', EditTextType::class, array(
                'toEdit' => 'Dopo che sono tornato dal viaggio in Giappone, la mia vita ha ripreso con il solito tram tram.'
            ))
            ->add('lessicoC', EditTextType::class, array(
                'toEdit' => 'Ho preso un altro 5: non sono molto afferrata in matematica.'
            ))
            ->add('lessicoD', EditTextType::class, array(
                'toEdit' => 'Hai un fratello più piccolo: perchè non li presti i tuoi giochi?'
            ))
            ->add('lessicoE', EditTextType::class, array(
                'toEdit' => 'Sei daccordo che, se studiassi di più, avresti risultati migliori?'
            ))
            ->add('lessicoF', EditTextType::class, array(
                'toEdit' => 'La nuova auto di mio papà ha un\'accellerazione potente.'
            ))
            ->add('lessicoG', EditTextType::class, array(
                'toEdit' => 'Sono stufo di andare sù e giù dalle scale per niente!'
            ))
            ->add('lessicoH', EditTextType::class, array(
                'toEdit' => 'L\'alimentari è d\'avanti alla Banca Popolare.'
            ))
            ->add('lessicoI', EditTextType::class, array(
                'toEdit' => 'Quest\'estate su gli scogli c\'era pieno di cozze.'
            ))
            ->add('lessicoJ', EditTextType::class, array(
                'toEdit' => 'Ho finito la colla: c\'è n\'è ancora nella scatola della cancelleria?'
            ))
            ->add('pluraleGuerra', EditTextInputType::class, array(
                'hint' => 'Guerra lampo'
            ))
            ->add('pluraleCapo', EditTextInputType::class, array(
                'hint' => 'Capoclasse'
            ))
            ->add('pluraleCane', EditTextInputType::class, array(
                'hint' => 'Cane poliziotto'
            ))
            ->add('pluraleBuono', EditTextInputType::class, array(
                'hint' => 'Buono sconto'
            ))
            ->add('funzioneGA', TextChoiceType::class, array(
                'choices'  => array(
                    'Pronome relativo' => 'A',
                    'Congiunzione' => 'B',
                ),
                'embed' => true,
                'question' => 'Vorrei tanto <em>che</em> un giorno i miei genitori mi portassero a Eurodisney.',
            ))
            ->add('funzioneGB', TextChoiceType::class, array(
                'choices'  => array(
                    'Preposizione impropria' => 'A',
                    'Avverbio' => 'B',
                ),
                'embed' => true,
                'question' => 'Non mi piace <em>assolutamente</em> fare la figura dell\'imbranato. ',
            ))
            ->add('funzioneGC', TextChoiceType::class, array(
                'choices'  => array(
                    'Articolo determinativo' => 'A',
                    'Pronome personale' => 'B',
                ),
                'embed' => true,
                'question' => 'Ho qui la spesa: dove <em>la</em> metto? ',
            ))
            ->add('funzioneLA', TextChoiceType::class, array(
                'choices'  => array(
                    'Complemento oggetto' => 'A',
                    'Soggetto' => 'B',
                ),
                'embed' => true,
                'question' => '<em>Le scarpe rosse</em> devi mettere, non quelle bianche!',
            ))
            ->add('funzioneLB', TextChoiceType::class, array(
                'choices'  => array(
                    'Soggetto' => 'A',
                    'Complemento di termine' => 'B',
                ),
                'embed' => true,
                'question' => 'Il vestito nero, in effetti, <em>mi</em> sembra più consono a una serata a teatro di uno a fiori.',
            ))
            ->add('funzioneLC', TextChoiceType::class, array(
                'choices'  => array(
                    'Complemento oggetto' => 'A',
                    'Soggetto' => 'B',
                ),
                'embed' => true,
                'question' => 'Ha raggiunto la cima anche <em>tuo cugino</em>?',
            ))
            ->add('funzioneSA', TextChoiceType::class, array(
                'choices'  => array(
                    'Relativa' => 'A',
                    'Oggettiva' => 'B',
                ),
                'embed' => true,
                'question' => 'Devo ancora finire di ricoprire i libri di scuola <em>che ho comprato a settembre</em>.',
            ))
            ->add('funzioneSB', TextChoiceType::class, array(
                'choices'  => array(
                    'Temporale' => 'A',
                    'Condizionale' => 'B',
                ),
                'embed' => true,
                'question' => 'Non sono certo che, <em>uscito di scuola</em>, andrò subito a casa a mangiare.',
            ))
            ->add('funzioneSC', TextChoiceType::class, array(
                'choices'  => array(
                    'Casuale' => 'A',
                    'Temporale' => 'B'
                ),
                'embed' => true,
                'question' => '<em>Dato che il tempo è ancora incerto</em>, abbiamo deciso di rimandare l\'escursione in montagna.',
            ))
            ->add('argomentoTesto', TextChoiceType::class, array(
                'choices'  => array(
                    'La morfologia dei territori prealpini' => 'A',
                    'Le difficoltà di andare in bicicletta nelle città prealpine' => 'B',
                    'La comodità e la bellezza di andare in giro in bicicletta' => 'C',
                    'La lentezza degli spostamenti su due ruote' => 'D',
                ),
                'question' => 'Qual è l\'argomento principale?'
            ))
            ->add('dolcemente', TextChoiceType::class, array(
                'choices'  => array(
                    'Dolcemente' => 'A',
                    'Con mitezza' => 'B',
                    'Malvolentieri' => 'C',
                    'Debolmente' => 'D',
                ),
                'question' => 'Il termine "docilmente" significa'
            ))
            ->add('nonCade', TextChoiceType::class, array(
                'choices'  => array(
                    'Non si sentì mai in colpa' => 'A',
                    'Non cadde mai in trappola' => 'B',
                    'Non s\'incolpò' => 'C',
                    'Non si sbagliò mai' => 'D',
                ),
                'question' => 'L\'espressione "non cadde mai in colpa" nel contesto significa: '
            ))
            ->add('sequenza', TextChoiceType::class, array(
                'choices'  => array(
                    'descrittiva' => 'A',
                    'narrativa' => 'B',
                    'dialogica' => 'C',
                    'riflessiva' => 'D',
                    'mista' => 'E'
                ),
                'question' => 'Il testo riportato si può definire una sequenza prevalentemente logica'
            ))
            ->add('tempo', TextChoiceType::class, array(
                'choices'  => array(
                    'brevissimo' => 'A',
                    'estremamente lungo' => 'B',
                    'della durata di un po\' di giorni' => 'C',
                ),
                'question' => 'Le azioni descrittive si svolgono in un tempo'
            ))
            ->add('spazio', TextChoiceType::class, array(
                'choices'  => array(
                    'arioso' => 'A',
                    'opprimente' => 'B',
                    'aperto' => 'C',
                    'rassicurante' => 'D',
                ),
                'question' => 'Lo spazio in cui si svolge il racconto ti dà l\'impressione che sia'
            ))
            ->add('narratore', TextChoiceType::class, array(
                'choices'  => array(
                    'Interno alla storia, cioè appartenente alla storia narrata' => 'A',
                    'Esterno, cioè non un personaggio che partecipa ai fatti narrati' => 'B',
                ),
                'question' => 'Il narratore è'
            ))
            ->add('buck', TextChoiceType::class, array(
                'choices'  => array(
                    'con rassegnazione' => 'A',
                    'con disperazione' => 'B',
                    'cercando di reagire alla situazione' => 'C',
                    'provando un sentimento di devozione nei confronti del suo padrone' => 'D'
                ),
                'question' => 'Il protagonista Buck reagisce a ciò che vede'
            ))
            ->add('lessico', TextChoiceType::class, array(
                'choices'  => array(
                    'di registro medio' => 'A',
                    'di registro elevato' => 'B',
                    'tecnico' => 'C',
                    'volgare' => 'D'
                ),
                'question' => 'Il lessico usato dall\'autore è'
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'consegna'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TestIta',
            'required' => false
        ));
    }
}
