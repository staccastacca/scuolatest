<?php
namespace AppBundle\Form\Type;

use AppBundle\Form\Type\TextChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class InfixChoiceType extends AbstractType
{

    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['embed1'] = true;
        $view->vars['noH'] = true; // noH does not render the question inside an H6
        $pieces = explode('#',$options['sentence']);
        $question = '<blockquote class="infixBlockQuote">' . $pieces[0];
        $nPieces = count($pieces);
        for($i = 1; $i < $nPieces; $i++){
            $question .= '<strong>[' . $i . ']</strong>' . $pieces[$i];
        }
        $view->vars['question'] = $question . '</blockquote>';
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'label' => false,
            'sentence' => 'Frase # di # esempio',
            'expanded' => true,
            'multiple' => false,
        ));
    }

    /**
     * Returns an array of choices for InfixChoiceType
     *
     * @param $length integer Number of choices
     * @return array
     */
    public static function generatePositionChoiceArray($length){
        $result = array();
        for($i = 1; $i <= $length; $i++)
            $result['Posizione ['.$i.']'] = $i;
        return $result;
    }

    public function getParent() {
        return TextChoiceType::class;
    }
}
