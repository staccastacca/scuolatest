<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class TextChoiceType extends AbstractType
{

    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['question'] = $options['question'];
        $view->vars['embed1'] = $options['embed'];
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'label' => false,
            'question' => 'Testo della domanda',
            'expanded' => true,
            'multiple' => false,
            'embed' => false,
        ));
    }

    public function getParent() {
        return ChoiceType::class;
    }

    public function getBlockPrefix() {
        return 'textchoice';
    }
}
