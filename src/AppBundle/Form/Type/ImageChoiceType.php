<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ImageChoiceType extends AbstractType
{

    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['base_path'] = $options['base_path'];
        $view->vars['question'] = $options['question'];
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'base_path' => 'http://placehold.it/700x400',
            'label' => false,
            'question' => 'Testo della domanda',
            'expanded' => true,
            'multiple' => false,
        ));
    }

    public function getParent() {
        return ChoiceType::class;
    }

    public function getBlockPrefix() {
        return 'imagechoice';
    }
}
