<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditTextInputType extends AbstractType
{

    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['value'] = $options['toEdit'];
        $view->vars['hint'] = $options['hint'];
        $view->vars['attr']['placeholder'] = $options['toEdit']; // just in case the user empties the input
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'label' => false,
            'toEdit' => '',
            'embed' => false,
            'hint' => '',
            'attr' => array(
                'autocomplete' => 'off',
                'spellcheck' => 'false',
                'autocorrect' => 'off',
            ),
        ));
    }

    public function getParent() {
        return TextType::class;
    }

    public function getBlockPrefix() {
        return 'edittextin';
    }
}
