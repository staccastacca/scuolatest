<?php

namespace AppBundle\Form;

use AppBundle\Form\Type\ImageChoiceType;
use AppBundle\Form\Type\TextChoiceType;
use AppBundle\Form\Type\TrueFalseType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FisicaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('localita',TextChoiceType::class, array(
              'choices'  => array(
                  'A metà percorso' => 'A',
                  'Quando il treno A ha percorso 2/5 della distanza totale' => 'B',
                  'Quando il treno A ha percorso 2/3 della distanza totale' => 'C',
                  'Quando il treno B ha percorso 2/3 della distanza totale' => 'D',
                  'Quando il treno B ha percorso 2/5 della distanza totale' => 'E',
              ),
              'question' => 'La località A dista dalla località B 675 Km. Dalle due città partono contemporanemente due treni, in direzione opposta, quello che va da A a B viaggia a 300 Km/h, quello che va da B ad A viaggia a 150Km/h. I due treni si incrociano:',
            ))
            ->add('luce',TextChoiceType::class, array(
              'choices'  => array(
                  '144 chilometri' => 'A',
                  '298 milioni di chilometri' => 'B',
                  '144 milioni di chilometri' => 'C',
                  '149 mila chilometri' => 'D',
                  '400 mila chilometri' => 'E',
              ),
              'question' => 'La luce impiega circa 8 minuti per arrivare sulla terra partendo dal sole. Tenendo conto che la sua velocità di 300 mila Km/s qual è la distanza terra-sole?',
            ))
            ->add('circuitoLuminoso',ImageChoiceType::class, array(
              'choices'  => array(
                  'La lampada C è la più luminosa' => 'A',
                  'Tutte e tre le lampade hanno la stessa luminosità' => 'B',
                  'La lampada A è più luminosa delle altre due' => 'C',
                  'Le lampade A e B sono più luminose della lampada C' => 'D',
                  'La lampada B è la meno luminosa di tutte' => 'E',
              ),

              'base_path' => 'assets/images/circuito.png',
              'question' => 'Il circuito in figura rappresenta due lampade A e B poste tra loro in parallelo e in serie ad una terza lampada C. Le tre lampade sono tutte uguali. Quali di queste frasi è corretta?',
            ))
            ->add('circuitoEsplode',TextChoiceType::class, array(
              'choices'  => array(
                  'Anche la lampada A si spegne' => 'A',
                  'Le lampade A e C diventano più luminose' => 'B',
                  'Le lampade C e A continuano a funzionare ma la luminosità di A cambia' => 'C',
                  'Nessuna frase è vera' => 'D',
              ),
              'question' =>'Nel disegno precedente brucia il filamento della lampada B (circuito aperto). Ovviamente la lampada B si spegne, ma cosa succede alle altre due? [Scegli l\'affermazione vera]'
            ))

            ->add('submit', SubmitType::class, array(
                'label' => 'consegna'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TestFisica',
            'required' => false
        ));
    }
}
