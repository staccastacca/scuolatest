![cose](./web/android-icon-48x48.png) ScuolaTest
==========
![fg](https://heroku-badge.herokuapp.com/?app=scuolatest)

Piattaforma per i test di autovalutazione dell'istituto *S. Ten. Vasc. A. Badoni*.

## Autori

Claudio Maggioni, Davide Fiori e Giorgio Croci.

## In breve

Questa applicazione web scritta in **PHP 7** e basata su **Symfony 3** è uno strumento utile sia
ai futuri alunni che ai professori:

- Gli **studenti** possono svolgere le prove per **misurare** le proprie
**capacità**, e valutare se essi siano in grado di **frequentare** il Badoni positivamente;

- I **docenti** ricevono anonimamente i risultati delle prove degli studenti e,
grazie alle **statistiche** generate, delineare un programma di **ripasso** ideale.

## Le prove

Ciascuna prova ha un **tempo** massimo di esecuzione al termine del quale le **risposte** date
dallo studente vengono registrate e **corrette**. Il lavoro dell'alunno viene **salvato**
automaticamente.

## Le statistiche

Scelta una materia, l'applicazione genera con le **prove consegnate** un prospetto che mostra,
domanda per domanda, un'**analisi** del **punteggio** ottenuto dagli alunni.

## Espansione e modifica

L'applicazione è stata progettata per essere **ampliata**. L'inserimento di nuove prove
è **facile** per un addetto ai lavori: è sufficiente definire solamente **domande, correzioni e
limite di tempo**. La logica di consegna e di **elaborazione** dei dati e **comune** a tutte le prove.

## Organizzazione del lavoro

Il lavoro è stato diviso nel modo seguente:

- **Backend**: logica interna dell'applicazione ed elaborazione dei risultati, sviluppata 
principalmente da Maggioni con aiuti da Croci e Fiori;

- **Frontend**: aspetto grafico con studio e realizzazione del modulo virtuale per le domande, 
realizzato principalmente da Croci con partecipazione di Fiori e Maggioni;

- **Immissione delle domande** nella piattaforma e adattamento di questa alla fruizione
in un modulo virtuale, fatto da Fiori.

## Conclusioni

La piattaforma funziona e soddisfa i requisiti per la sua facilità d'uso. La divisione
del lavoro è stata fatta equamente, sulla base delle capacità di ciascuno.
